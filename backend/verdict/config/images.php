<?php

return [
    'userPhotoWidth' => 150,
    'userPhotoHeight' => 150,

    'channelPhotoWidth' => 450,
    'channelPhotoHeight' => 450,

    'projectPhotoWidth' => 450,
    'projectPhotoHeight' => 450,
];
