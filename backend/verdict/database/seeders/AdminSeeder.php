<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User();
        $admin->name = 'Администратор';
        $admin->email = env('ADMIN_EMAIL');
        $admin->password = Hash::make(env('ADMIN_PASSWORD'));
        $admin->email_verified_at = Carbon::now()->toDateTimeString();
        $admin->role = Role::ADMIN()->getValue();
        $admin->save();
    }
}
