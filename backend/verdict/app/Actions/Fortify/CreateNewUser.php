<?php

namespace App\Actions\Fortify;

use App\Enums\Gender;
use App\Enums\Role;
use App\Interfaces\ICacheService;
use App\Models\Channel;
use App\Models\Participant;
use App\Models\User;
use App\Utils\MediaFilesHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param array $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        $baseImagePattern = "/^data:image\/(jpeg|png);base64,/";
        $newUserRole = $input['role'];
        Validator::make($input, [
            'name' => ['required', 'string', 'max:50'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
            'role' => [
                Rule::in([
                    Role::PARTICIPANT()->getValue(),
                    Role::MANAGER()->getValue()
                ])
            ],
            'photo' => ['required', 'string', "regex:$baseImagePattern"],
            'city' => [
                'nullable',
                'string',
                'max:50',
                Rule::requiredIf(fn() => $newUserRole == Role::PARTICIPANT())
            ],
            'gender' => [
                'nullable',
                'string',
                Rule::requiredIf(fn() => $newUserRole == Role::PARTICIPANT()),
                Rule::in([
                    Gender::MALE()->getValue(),
                    Gender::FEMALE()->getValue()
                ])
            ],
            'birthDate' => [
                'nullable',
                'string',
                'date_format:Y-m-d',
                Rule::requiredIf(fn() => $newUserRole == Role::PARTICIPANT()),
                'before:now'
            ],
            'channelName' => [
                'nullable',
                'string',
                'max:50',
                Rule::requiredIf(fn() => $newUserRole == Role::MANAGER())
            ],
            'channelDescription' => [
                'nullable',
                'string',
                'max:750',
                Rule::requiredIf(fn() => $newUserRole == Role::MANAGER())
            ],
            'channelPhotos' => [
                'nullable',
                'array',
                'min:1',
                'max:3',
                Rule::requiredIf(fn() => $newUserRole == Role::MANAGER())
            ],
            'channelPhotos.*' => ['string', "regex:$baseImagePattern"]
        ])->validate();

        DB::transaction(function() use ($input, $newUserRole) {
            $newUser = User::create([
                'name' => $input['name'],
                'email' => $input['email'],
                'password' => Hash::make($input['password']),
                'role' => $newUserRole,
                'photo_path' => MediaFilesHelper::saveImageOnDisk(
                    $input['photo'],
                    'uploads/user-photos/',
                    config('images.userPhotoWidth'),
                    config('images.userPhotoHeight')
                )
            ]);
            Log::info('Created new user', [
                'newUser' => $newUser
            ]);
            if ($newUserRole == Role::PARTICIPANT()) {
                $participant = new Participant();
                $participant->city = $input['city'];
                $participant->gender = $input['gender'];
                $participant->birth_date = Carbon::createFromIsoFormat('Y-MM-DD', $input['birthDate'])
                    ->startOfDay()
                    ->toDateTimeString();
                $participant->user_id = $newUser->id;
                $participant->save();
                Log::info('Created new participant record', [
                    'newParticipant' => $participant
                ]);
            } elseif ($newUserRole == Role::MANAGER()) {
                $channel = new Channel();
                $channel->name = $input['channelName'];
                $channelPhotoPaths = [];
                foreach ($input['channelPhotos'] as $photo) {
                    $channelPhotoPaths[] = MediaFilesHelper::saveImageOnDisk(
                        $photo,
                        'uploads/channel-photos/',
                        config('images.channelPhotoWidth'),
                        config('images.channelPhotoHeight')
                    );
                }
                $channel->photo_paths = json_encode($channelPhotoPaths);
                $channel->description = $input['channelDescription'];
                $channel->user_id = $newUser->id;
                $channel->save();
                Log::info('Created new channel record', [
                    'newChannel' => $channel
                ]);
                /**@var ICacheService $cacheService*/
                $cacheService = App::make(ICacheService::class);
                $cacheService->syncChannelsListForSelect(null);
            }
        });

        /**@var User $createdUser*/
        $createdUser = User::query()
            ->where('email', $input['email'])
            ->firstOrFail();
        return $createdUser;
    }
}
