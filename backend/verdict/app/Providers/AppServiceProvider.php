<?php

namespace App\Providers;

use App\Interfaces\IApplicationService;
use App\Interfaces\ICacheService;
use App\Interfaces\IChannelService;
use App\Interfaces\ICityService;
use App\Interfaces\IProjectService;
use App\Interfaces\IStatisticsService;
use App\Interfaces\IUserService;
use App\Services\ApplicationService;
use App\Services\CacheService;
use App\Services\ChannelService;
use App\Services\CityService;
use App\Services\ProjectService;
use App\Services\StatisticsService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        IApplicationService::class => ApplicationService::class,
        ICacheService::class => CacheService::class,
        IChannelService::class => ChannelService::class,
        ICityService::class => CityService::class,
        IProjectService::class => ProjectService::class,
        IStatisticsService::class => StatisticsService::class,
        IUserService::class => UserService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
