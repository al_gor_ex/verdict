<?php


namespace App\Exceptions;


use Exception;

class WrongProjectOwnerException extends Exception
{
    public function __construct(
        private int $ownerId,
        private int $realId
    )
    {
        parent::__construct('User has no permission to update the project', 106);
    }

    public function context()
    {
        return [
            'ownerId' => $this->ownerId,
            'realId' => $this->realId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => "Отказано в обновлении проекта пользователю {$this->realId}. Владелец: {$this->ownerId}",
            'detail' => "User {$this->realId} cannot change the project owned by {$this->ownerId}"
        ], 403);
    }
}
