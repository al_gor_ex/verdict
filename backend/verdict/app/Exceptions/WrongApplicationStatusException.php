<?php


namespace App\Exceptions;


use App\Enums\ApplicationStatus;
use Exception;

class WrongApplicationStatusException extends Exception
{
    public function __construct(
        private ApplicationStatus $currentStatus,
        private ApplicationStatus $unreachableStatus
    )
    {
        parent::__construct('Wrong application status', 102);
    }

    public function context()
    {
        return [
            'currentStatus' => $this->currentStatus->getValue(),
            'unreachableStatus' => $this->unreachableStatus->getValue()
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => "Невозможно изменить статус заявки с {$this->currentStatus->getValue()} на {$this->unreachableStatus->getValue()}",
            'detail' => "Cannot change application status from {$this->currentStatus->getValue()} to {$this->unreachableStatus->getValue()}"
        ], 422);
    }
}
