<?php


namespace App\Exceptions;


use Exception;

class DuplicateApplicationException extends Exception
{

    public function __construct(private int $projectId, private int $participantId
    )
    {
        parent::__construct('Duplicate application', 100);
    }

    public function context()
    {
        return [
            'projectId' => $this->projectId,
            'participantId' => $this->participantId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => 'Заявка на проект уже подана',
            'detail' => 'Duplicate application'
        ], 422);
    }
}
