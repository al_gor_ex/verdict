<?php


namespace App\Exceptions;


use Exception;

class WrongApplicationVideoIdException extends Exception
{
    public function __construct(
        private string $videoId
    )
    {
        parent::__construct("Cannot find video file in storage", 105);
    }

    public function context()
    {
        return [
            'videoId' => $this->videoId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => 'Видеофайл не найден',
            'detail' => 'Cannot find video file'
        ], 404);
    }
}
