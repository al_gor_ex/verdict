<?php


namespace App\Exceptions;


use Exception;
use JetBrains\PhpStorm\ArrayShape;

class WrongApplicationVideoOwnerException extends Exception
{
    public function __construct(
        private string $videoId
    )
    {
        parent::__construct("User has no permission to view the application video", 104);
    }

    public function context()
    {
        return [
            'videoId' => $this->videoId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => 'Текущий пользователь не обладает правами для просмотра данного видео',
            'detail' => 'Current user has no permission to view this video'
        ], 403);
    }
}
