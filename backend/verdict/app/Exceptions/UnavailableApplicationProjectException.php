<?php


namespace App\Exceptions;


use Exception;

class UnavailableApplicationProjectException extends Exception
{
    public function __construct(private int $projectId)
    {
        parent::__construct('Project is unavailable', 101);
    }

    public function context()
    {
        return [
            'projectId' => $this->projectId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => 'Проект недоступен для подачи заявки',
            'detail' => 'Project is unavailable for creating an application'
        ], 422);
    }
}
