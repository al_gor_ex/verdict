<?php


namespace App\Exceptions;


use Exception;

class WrongApplicationOwnerException extends Exception
{
    public function __construct(
        private int $ownerId,
        private int $realId
    )
    {
        parent::__construct('User has no permission to change the application', 103);
    }

    public function context()
    {
        return [
            'ownerId' => $this->ownerId,
            'realId' => $this->realId
        ];
    }

    public function render()
    {
        return response()->json([
            'code' => $this->code,
            'message' => "Отказано в изменении заявки пользователю {$this->realId}. Владелец: {$this->ownerId}",
            'detail' => "User {$this->realId} cannot change application owned by {$this->ownerId}"
        ], 403);
    }
}
