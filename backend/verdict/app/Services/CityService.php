<?php


namespace App\Services;


use App\Interfaces\ICityService;
use App\Models\Project;

class CityService implements ICityService
{

    /** @return string[] */
    public function getCitiesListForSelect(): array
    {
        return Project::all()
            ->map(fn(Project $pr) => $pr->city)
            ->unique()
            ->values()
            ->sort()
            ->values()
            ->toArray();
    }
}
