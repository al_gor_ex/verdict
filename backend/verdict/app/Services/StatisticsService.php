<?php


namespace App\Services;


use App\Enums\ApplicationStatus;
use App\Http\Dto\View\ApplicationsCountViewModel;
use App\Http\Dto\View\ChannelPopularityViewModel;
use App\Http\Dto\View\StatisticsViewModel;
use App\Interfaces\IStatisticsService;
use App\Models\Application;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class StatisticsService implements IStatisticsService
{
    public function getStatsForCharts(): StatisticsViewModel
    {
        return new StatisticsViewModel(
            $this->getApplicationsCountPerDay(),
            $this->getChannelsPopularity()
        );
    }

    /** @return ApplicationsCountViewModel[] */
    private function getApplicationsCountPerDay(): array
    {
        $result = [];
        $daysCount = config('statistics.newApplicationsDaysCount');
        $curDateCrb = Carbon::now()->startOfDay();
        for ($i = 0; $i < $daysCount; $i++) {
            $startOfCurDay = $curDateCrb->toDateTimeString();
            $endOfCurDay = (clone $curDateCrb)->addDay()->toDateTimeString();
            $applicationsCount = Application::query()
                ->whereBetween('created_at', [$startOfCurDay, $endOfCurDay])
                ->where('status', '<>', ApplicationStatus::CANCELED()->getValue())
                ->count();
            $dayName = $curDateCrb->isoFormat('D MMM');
            $result[] = new ApplicationsCountViewModel($dayName, $applicationsCount);
            $curDateCrb->subDay();
        }
        $result = array_reverse($result);
        return $result;
    }

    /** @return ChannelPopularityViewModel[] */
    private function getChannelsPopularity(): array
    {
        $statistics = DB::table('channels AS C')
            ->join('projects AS P', 'P.channel_id', '=', 'C.id')
            ->join('applications AS A', 'A.project_id', '=', 'P.id')
            ->where('A.status', '<>', ApplicationStatus::CANCELED()->getValue())
            ->selectRaw('C.name AS channel_name, COUNT(*) AS applications_count')
            ->groupBy('channel_name')
            ->orderBy('applications_count', 'DESC')
            ->get();
        $result = [];
        foreach ($statistics as $channelStat) {
            $result[] = new ChannelPopularityViewModel(
                $channelStat->channel_name,
                $channelStat->applications_count
            );
        }
        $maxChannelsCount = config('statistics.channelsPopularityMaxItems');
        $result = collect($result);
        if (count($result) > $maxChannelsCount) {
            // Reduce extra channels into one "other" piece
            $otherChannels = $result->splice($maxChannelsCount);
            $otherChannelsApplicationsCount = $otherChannels->reduce(
                fn ($accum, ChannelPopularityViewModel $ch) => $accum + $ch->percent,
                0
            );
            $result->add(new ChannelPopularityViewModel('Прочие', $otherChannelsApplicationsCount));
        }
        // Convert absolute values to percents.
        $totalApplicationsCount = $result->reduce(
            fn ($accum, ChannelPopularityViewModel $ch) => $accum + $ch->percent,
            0
        );
        $result = $result->map(function(ChannelPopularityViewModel $ch) use ($totalApplicationsCount) {
             $ch->percent = ceil($ch->percent / $totalApplicationsCount * 100);
             return $ch;
        });
        $result = $result->toArray();
        return $result;
    }
}
