<?php


namespace App\Services;


use App\Enums\ApplicationStatus;
use App\Enums\Role;
use App\Exceptions\WrongProjectOwnerException;
use App\Http\Dto\Binding\ProjectBindingModel;
use App\Http\Dto\Binding\ProjectsListFiltersBindingModel;
use App\Http\Dto\View\ProjectEditorViewModel;
use App\Http\Dto\View\ProjectListViewModel;
use App\Http\Dto\View\ProjectParticipantsCountModel;
use App\Http\Dto\View\ProjectsListViewModel;
use App\Interfaces\IApplicationService;
use App\Interfaces\ICacheService;
use App\Interfaces\IProjectService;
use App\Models\Application;
use App\Models\Project;
use App\Models\User;
use App\Utils\MediaFilesHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProjectService implements IProjectService
{

    public function __construct(
        private IApplicationService $applicationService,
        private ICacheService $cacheService
    )
    {
    }


    public function createProject(User $user, ProjectBindingModel $model): void
    {
        $project = new Project();
        $project->name = $model->name;
        $photoPaths = [];
        foreach ($model->photos as $photo) {
            $photoPaths[] = MediaFilesHelper::saveImageOnDisk(
                $photo,
                'uploads/project-photos/',
                config('images.projectPhotoWidth'),
                config('images.projectPhotoHeight')
            );
        }
        $project->photo_paths = json_encode($photoPaths);
        $project->description = $model->description;
        $project->city = $model->city;
        $project->participants_limit = $model->participantsLimit;
        $project->fee = $model->fee;
        $project->starts_at = Carbon::createFromIsoFormat('Y-MM-DD', $model->startsAt)
            ->startOfDay()
            ->toDateTimeString();
        $user->channel->projects()->save($project);
        Log::info('Created new project', [
            'userId' => $user->id,
            'newProject' => $project
        ]);
        $this->cacheService->syncCitiesListForSelect(null);
    }

    public function getProjectsList(?User $user, ProjectsListFiltersBindingModel $filters): ProjectsListViewModel
    {
        $projects = Project::all();
        if (isset($filters->channelId)) {
            $projects = $projects->filter(fn (Project $pr) => $pr->channel_id == $filters->channelId);
        }
        if (isset($filters->city)) {
            $projects = $projects->filter(fn (Project $pr) => $pr->city == $filters->city);
        }
        if (isset($filters->onlyFee)) {
            $projects = $projects->filter(fn (Project $pr) => $pr->fee !== null);
        }
        if (isset($filters->onlyAvailable)) {
            $projects = $projects->filter(fn (Project $pr) => $this->applicationService->isProjectAvailable($pr->id));
        }
        if (isset($filters->searchName)) {
            $filters->searchName = Str::lower($filters->searchName);
            $projects = $projects->filter(
                fn (Project $pr) => Str::contains(Str::lower($pr->name), $filters->searchName)
            );
        }
        $pagesCount = ceil(count($projects) / $filters->pageSize);
        if ($filters->page > $pagesCount) {
            $filters->page = $pagesCount;
        }
        $projects = $projects->forPage($filters->page, $filters->pageSize);
        $result = new ProjectsListViewModel();
        $result->pagesCount = $pagesCount;
        foreach ($projects as $project) {
            $photos = MediaFilesHelper::loadImagesFromDisk(json_decode($project->photo_paths));
            $startsCarbon = Carbon::createFromTimeString($project->starts_at);
            $startDate = $startsCarbon->isoFormat('D MMMM Y');
            $startDiff = $startsCarbon->diffForHumans();
            $startsAt = "$startDate ($startDiff)";
            $approvedParticipantsCount = Application::query()
                ->where('project_id', $project->id)
                ->where('status', ApplicationStatus::APPROVED()->getValue())
                ->count();
            $isAvailable = (isset($user) == false || $user->role == Role::PARTICIPANT()) ?
                $this->applicationService->isProjectAvailable($project->id) :
                null;
            $hasApplied = (isset($user) && $user->role == Role::PARTICIPANT()) ?
                $this->applicationService->hasAlreadyApplied($project->id, $user->participant->id) :
                null;
            $canEdit = (isset($user) && $user->role == Role::MANAGER()) ?
                ($project->channel_id == $user->channel->id) :
                null;
            $result->projects[] = new ProjectListViewModel(
                $project->id,
                $project->name,
                $project->description,
                $photos,
                $project->channel->name,
                $project->city,
                $startsAt,
                new ProjectParticipantsCountModel($approvedParticipantsCount, $project->participants_limit),
                $project->fee,
                $isAvailable,
                $hasApplied,
                $canEdit
            );
        }
        return $result;
    }

    public function getProjectForEditing(int $id): ProjectEditorViewModel
    {
        /**@var Project $project*/
        $project = Project::query()->findOrFail($id);
        $startDate = explode(' ', $project->starts_at)[0];
        $approvedParticipantsCount = Application::query()
            ->where('project_id', $id)
            ->where('status', ApplicationStatus::APPROVED()->getValue())
            ->count();
        return new ProjectEditorViewModel(
            $project->name,
            $project->description,
            $project->city,
            $approvedParticipantsCount,
            $project->participants_limit,
            $project->fee,
            $startDate
        );
    }

    public function updateProject(User $user, ProjectBindingModel $model)
    {
        /**@var $project Project*/
        $project = Project::query()->findOrFail($model->id);
        if ($this->isProjectOwner($user, $project) == false) {
            throw new WrongProjectOwnerException(
                $project->channel->user_id,
                $user->id
            );
        }
        $project->name = $model->name;
        $project->description = $model->description;
        $project->city = $model->city;
        $project->participants_limit = $model->participantsLimit;
        $project->fee = $model->fee;
        $project->starts_at = Carbon::createFromIsoFormat('Y-MM-DD', $model->startsAt)
            ->startOfDay()
            ->toDateTimeString();
        // If user has uploaded new photos, delete all old photos and save the new ones.
        if (count($model->photos) > 0) {
            MediaFilesHelper::deleteImagesFromDisk(json_decode($project->photo_paths));
            $photoPaths = [];
            foreach ($model->photos as $photo) {
                $photoPaths[] = MediaFilesHelper::saveImageOnDisk(
                    $photo,
                    'uploads/project-photos/',
                    config('images.projectPhotoWidth'),
                    config('images.projectPhotoHeight')
                );
            }
            $project->photo_paths = json_encode($photoPaths);
        }
        DB::transaction(function() use ($project, $user) {
            $project->save();
            Log::info('Updated project', [
                'userId' => $user->id,
                'newProjectData' => $project
            ]);
            if ($this->applicationService->anyFreePlaces($project->id) == false) {
                Log::info('No more places after project update. Declining other pending applications', [
                    'projectId' => $project->id
                ]);
                $this->applicationService->declineAllPendingApplications($user, $project->id);
            }
        });
        $this->cacheService->syncCitiesListForSelect(null);
    }

    private function isProjectOwner(User $user, Project $project): bool
    {
        return ($project->channel->user_id == $user->id);
    }
}
