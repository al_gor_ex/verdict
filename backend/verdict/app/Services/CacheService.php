<?php


namespace App\Services;


use App\Exceptions\CacheMissException;
use App\Http\Dto\View\ChannelSelectViewModel;
use App\Http\Dto\View\StatisticsViewModel;
use App\Interfaces\ICacheService;
use App\Interfaces\IChannelService;
use App\Interfaces\ICityService;
use App\Interfaces\IStatisticsService;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Predis\Client;

class CacheService implements ICacheService
{
    private Client $redis;
    private const CHANNELS_SELECT_LIST_KEY = 'channelsSelectList';
    private const CITIES_SELECT_LIST_KEY = 'citiesSelectList';
    private const STATS_FOR_CHARTS_KEY = 'statsForCharts';

    public function __construct()
    {
        $this->redis = new Client();
        $this->redis->auth(config('database.redis.default.password'));
    }

    /**
     * @return ChannelSelectViewModel[]
     * @throws CacheMissException
     */
    public function getChannelsListForSelect(): array
    {
        if ($this->redis->exists(static::CHANNELS_SELECT_LIST_KEY) == false) {
            throw new CacheMissException();
        }
        return unserialize($this->redis->get(static::CHANNELS_SELECT_LIST_KEY));
    }

    /**
     * @return string[]
     * @throws CacheMissException
     */
    public function getCitiesListForSelect(): array
    {
        if ($this->redis->exists(static::CITIES_SELECT_LIST_KEY) == false) {
            throw new CacheMissException();
        }
        return unserialize($this->redis->get(static::CITIES_SELECT_LIST_KEY));
    }

    /**
     * @throws CacheMissException
     */
    public function getStatsForCharts(): StatisticsViewModel
    {
        if ($this->redis->exists(static::STATS_FOR_CHARTS_KEY) == false) {
            throw new CacheMissException();
        }
        return unserialize($this->redis->get(static::STATS_FOR_CHARTS_KEY));
    }

    /**
     * @param ChannelSelectViewModel[]|null $freshData
     */
    public function syncChannelsListForSelect(?array $freshData): void
    {
        if (is_null($freshData)) {
            /**@var IChannelService $channelsService*/
            $channelsService = App::make(IChannelService::class);
            $freshData = $channelsService->getChannelsListForSelect();
        }
        $this->redis->set(static::CHANNELS_SELECT_LIST_KEY, serialize($freshData));
    }

    /**
     * @param string[]|null $freshData
     */
    public function syncCitiesListForSelect(?array $freshData): void
    {
        if (is_null($freshData)) {
            /**@var ICityService $cityService*/
            $cityService = App::make(ICityService::class);
            $freshData = $cityService->getCitiesListForSelect();
        }
        $this->redis->set(static::CITIES_SELECT_LIST_KEY, serialize($freshData));
    }


    public function syncStatsForCharts(?StatisticsViewModel $freshData): void
    {
        if (is_null($freshData)) {
            /**@var IStatisticsService $statsService*/
            $statsService = App::make(IStatisticsService::class);
            $freshData = $statsService->getStatsForCharts();
        }
        $this->redis->set(static::STATS_FOR_CHARTS_KEY, serialize($freshData));
        $expirationStamp = Carbon::now()->addDay()->startOfDay()->timestamp;
        $this->redis->expireat(static::STATS_FOR_CHARTS_KEY, $expirationStamp);
    }
}
