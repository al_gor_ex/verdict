<?php


namespace App\Services;


use App\Http\Dto\View\UserViewModel;
use App\Interfaces\IUserService;
use App\Models\User;
use App\Utils\MediaFilesHelper;

class UserService implements IUserService
{

    public function getUserInfo(User $user): UserViewModel
    {
        return new UserViewModel(
            $user->name,
            $user->role,
            ($user->photo_path !== null) ? MediaFilesHelper::loadImageFromDisk($user->photo_path) : ''
        );
    }
}
