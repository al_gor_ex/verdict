<?php


namespace App\Services;


use App\Enums\ApplicationSortingField;
use App\Enums\ApplicationStatus;
use App\Enums\Role;
use App\Enums\SortingDirection;
use App\Exceptions\DuplicateApplicationException;
use App\Exceptions\UnavailableApplicationProjectException;
use App\Exceptions\WrongApplicationOwnerException;
use App\Exceptions\WrongApplicationStatusException;
use App\Exceptions\WrongApplicationVideoIdException;
use App\Exceptions\WrongApplicationVideoOwnerException;
use App\Http\Dto\Binding\ApplicationBindingModel;
use App\Http\Dto\Binding\ApplicationsListFiltersBindingModel;
use App\Http\Dto\View\ApplicationListViewModel;
use App\Http\Dto\View\ApplicationManagerViewModel;
use App\Http\Dto\View\ApplicationParticipantViewModel;
use App\Http\Dto\View\ApplicationsListViewModel;
use App\Http\Dto\View\VideoViewModel;
use App\Interfaces\IApplicationService;
use App\Interfaces\ICacheService;
use App\Mail\ApplicationViewed;
use App\Mail\NewApplicationCreated;
use App\Models\Application;
use App\Models\Project;
use App\Models\User;
use App\Utils\MediaFilesHelper;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ApplicationService implements IApplicationService
{
    public function __construct(
        private ICacheService $cacheService
    )
    {
    }

    /**
     * @inheritDoc
     */
    public function createApplication(User $user, ApplicationBindingModel $model): int
    {
        if ($this->hasAlreadyApplied($model->projectId, $user->participant->id)) {
            throw new DuplicateApplicationException($model->projectId, $user->participant->id);
        }
        if ($this->isProjectAvailable($model->projectId) == false) {
            throw new UnavailableApplicationProjectException($model->projectId);
        }
        $application = new Application();
        $application->description = $model->description;
        $application->video_id = MediaFilesHelper::saveVideoOnDisk(
            $model->video,
            'uploads/application-videos/'
        );
        $application->status = ApplicationStatus::PENDING();
        $application->project_id = $model->projectId;
        $user->participant->applications()->save($application);
        Log::info('Created new application', [
            'userId' => $user->id,
            'newApplication' => $application
        ]);
        if (config('mail.enableMailing') == true) {
            $managerEmail = $application->project->channel->user->email;
            Mail::to($managerEmail)->send(new NewApplicationCreated(
                $application->id,
                $application->project->name,
                $application->participant->user->name
            ));
        }
        $this->cacheService->syncStatsForCharts(null);
        return $application->id;
    }

    public function getApplicationsList(
        User $user,
        ApplicationsListFiltersBindingModel $filters
    ): ApplicationsListViewModel
    {
        $appsQuery = Application::query();
        if (isset($filters->onlyPending) && $filters->onlyPending) {
            $appsQuery = $appsQuery->where('status', ApplicationStatus::PENDING()->getValue());
        }
        /**@var $applications Application[] | Collection */
        $applications = $appsQuery->get();
        switch ($filters->sortingField) {
            case ApplicationSortingField::CREATED_AT():
                $applications = $applications->sortBy(
                    'created_at',
                    descending: $filters->sortingDirection == SortingDirection::DESC()
                );
                break;
            case ApplicationSortingField::STATUS():
                $applications = $applications->sortBy(
                    'status',
                    descending: $filters->sortingDirection == SortingDirection::DESC()
                );
                break;
            case ApplicationSortingField::PARTICIPANT_NAME():
                $applications = $applications->sortBy(
                    [
                        fn(Application $a, Application $b) => ($filters->sortingDirection == SortingDirection::ASC()) ?
                            $a->participant->user->name <=> $b->participant->user->name :
                            -1 * ($a->participant->user->name <=> $b->participant->user->name)
                    ]
                );
                break;
            case ApplicationSortingField::PROJECT_NAME():
                $applications = $applications->sortBy(
                    [
                        fn(Application $a, Application $b) => ($filters->sortingDirection == SortingDirection::ASC()) ?
                            $a->project->name <=> $b->project->name :
                            -1 * ($a->project->name <=> $b->project->name)
                    ]
                );
                break;
        }
        if ($user->role == Role::MANAGER()) {
            // Manager should get only applications to his projects.
            $applications = $applications->filter(
                fn(Application $app) => $app->project->channel->user->id == $user->id
            );
        } else {
            // Participant should get only his applications.
            $applications = $applications->filter(
                fn(Application $app) => $app->participant->user->id == $user->id
            );
        }
        $result = new ApplicationsListViewModel();
        foreach ($applications as $application) {
            $createdAt = Carbon::createFromTimeString($application->created_at)->isoFormat('D.MM.Y');
            // Manager should see info about participant.
            $participant = null;
            if ($user->role == Role::MANAGER()) {
                $appParticipant = $application->participant;
                $age = Carbon::createFromTimeString($appParticipant->birth_date)->age;
                $participant = new ApplicationParticipantViewModel(
                    $appParticipant->user->name,
                    $appParticipant->user->email,
                    $appParticipant->gender,
                    $age,
                    $appParticipant->city,
                    MediaFilesHelper::loadImageFromDisk($appParticipant->user->photo_path)
                );
            }
            // Participant should see info about project's manager.
            $manager = null;
            if ($user->role == Role::PARTICIPANT()) {
                $appManager = $application->project->channel->user;
                $manager = new ApplicationManagerViewModel(
                    $appManager->name,
                    $appManager->channel->name,
                    MediaFilesHelper::loadImageFromDisk($appManager->photo_path)
                );
            }
            $result->applications[] = new ApplicationListViewModel(
                $application->id,
                $createdAt,
                $application->status,
                $application->project->name,
                $application->description,
                $application->video_id,
                $manager,
                $participant
            );
        }
        return $result;
    }

    public function getApplicationVideo(User $user, string $id): VideoViewModel
    {
        if ($this->isVideoOwner($user, $id) == false) {
            throw new WrongApplicationVideoOwnerException($id);
        }
        try {
            $video = MediaFilesHelper::loadVideoFromDisk('uploads/application-videos/', $id);
        } catch (FileNotFoundException) {
            throw new WrongApplicationVideoIdException($id);
        }
        return new VideoViewModel($video);
    }

    public function cancelApplication(User $user, int $id): void
    {
        /**@var Application $application */
        $application = Application::query()->findOrFail($id);
        if ($application->status != ApplicationStatus::PENDING()) {
            throw new WrongApplicationStatusException(
                new ApplicationStatus($application->status),
                ApplicationStatus::CANCELED()
            );
        }
        if ($user->participant->id != $application->participant->id) {
            throw new WrongApplicationOwnerException(
                $application->participant->user->id,
                $user->id
            );
        }
        $application->status = ApplicationStatus::CANCELED();
        $application->save();
        Log::info('Canceled application', [
            'userId' => $user->id,
            'applicationId' => $application->id
        ]);
        $this->cacheService->syncStatsForCharts(null);
    }

    public function approveApplication(User $user, int $id): void
    {
        /**@var Application $application */
        $application = Application::query()->findOrFail($id);
        if ($application->status != ApplicationStatus::PENDING()) {
            throw new WrongApplicationStatusException(
                new ApplicationStatus($application->status),
                ApplicationStatus::APPROVED()
            );
        }
        if ($user->id != $application->project->channel->user->id) {
            throw new WrongApplicationOwnerException(
                $application->project->channel->user->id,
                $user->id
            );
        }
        DB::transaction(function() use ($application, $user) {
            $application->status = ApplicationStatus::APPROVED();
            $application->save();
            Log::info('Approved application', [
                'userId' => $user->id,
                'applicationId' => $application->id
            ]);
            $projectId = $application->project_id;
            if ($this->anyFreePlaces($projectId) == false) {
                Log::info('No more places. Declining other pending applications', [
                    'projectId' => $projectId
                ]);
                $this->declineAllPendingApplications($user, $projectId);
            }
        });
        if (config('mail.enableMailing') == true) {
            Mail::to($application->participant->user->email)->send(new ApplicationViewed(
                $application->id,
                ApplicationStatus::APPROVED(),
                $application->project->name,
                $application->project->channel->name
            ));
        }
    }

    public function declineApplication(User $user, int $id): void
    {
        /**@var Application $application */
        $application = Application::query()->findOrFail($id);
        if ($application->status != ApplicationStatus::PENDING()) {
            throw new WrongApplicationStatusException(
                new ApplicationStatus($application->status),
                ApplicationStatus::DECLINED()
            );
        }
        if ($user->id != $application->project->channel->user->id) {
            throw new WrongApplicationOwnerException(
                $application->project->channel->user->id,
                $user->id
            );
        }
        $application->status = ApplicationStatus::DECLINED();
        $application->save();
        Log::info('Declined application', [
            'userId' => $user->id,
            'applicationId' => $application->id
        ]);
        if (config('mail.enableMailing') == true) {
            Mail::to($application->participant->user->email)->send(new ApplicationViewed(
                $application->id,
                ApplicationStatus::DECLINED(),
                $application->project->name,
                $application->project->channel->name
            ));
        }
    }

    public function hasAlreadyApplied(int $projectId, int $participantId): bool
    {
        $applicationsCount = Application::query()
            ->where('project_id', $projectId)
            ->where('participant_id', $participantId)
            ->where('status', '<>', ApplicationStatus::CANCELED()->getValue())
            ->count();
        return ($applicationsCount > 0);
    }

    public function anyFreePlaces(int $projectId): bool
    {
        /**@var $project Project */
        $project = Project::query()->findOrFail($projectId);
        $approvedApplicationsCount = Application::query()
            ->where('project_id', $projectId)
            ->where('status', ApplicationStatus::APPROVED()->getValue())
            ->count();
        return ($approvedApplicationsCount < $project->participants_limit);
    }

    public function declineAllPendingApplications(User $user, int $projectId): void
    {
        Application::query()
            ->where('project_id', $projectId)
            ->where('status', ApplicationStatus::PENDING()->getValue())
            ->get()
            ->map(
                fn(Application $app) => $this->declineApplication($user, $app->id)
            );
    }

    public function isProjectAvailable(int $projectId): bool
    {
        /**@var $project Project */
        $project = Project::query()->findOrFail($projectId);
        // If already started
        if (Carbon::createFromTimeString($project->starts_at) < Carbon::now()) {
            return false;
        }
        // If no places
        if ($this->anyFreePlaces($projectId) == false) {
            return false;
        }
        return true;
    }

    private function isVideoOwner(User $user, string $videoId): bool
    {
        /**@var Application $application */
        $application = null;
        try {
            $application = Application::query()
                ->where('video_id', $videoId)
                ->firstOrFail();
        } catch (ModelNotFoundException) {
            return false;
        }
        if ($user->role == Role::PARTICIPANT() && $application->participant->user_id !== $user->id) {
            return false;
        }
        if ($user->role == Role::MANAGER() && $application->project->channel->user_id !== $user->id) {
            return false;
        }
        return true;
    }
}
