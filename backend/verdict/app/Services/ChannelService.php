<?php


namespace App\Services;


use App\Enums\Role;
use App\Http\Dto\View\ChannelListViewModel;
use App\Http\Dto\View\ChannelSelectViewModel;
use App\Http\Dto\View\ChannelsListViewModel;
use App\Interfaces\IChannelService;
use App\Models\Channel;
use App\Models\User;
use App\Utils\MediaFilesHelper;
use Illuminate\Support\Str;

class ChannelService implements IChannelService
{

    public function getChannelsList(?User $user, int $page, int $pageSize, ?string $searchName = null): ChannelsListViewModel
    {
        $channels = Channel::all();
        if (isset($searchName)) {
            $searchName = Str::lower($searchName);
            $channels = $channels->filter(fn (Channel $ch) => Str::contains(Str::lower($ch->name), $searchName));
        }
        // Current manager's channel must be the first in result (if it's present)
        if (isset($user) && $user->role == Role::MANAGER() && count($channels) > 0) {
            $managedChannel = $channels->firstWhere('user_id', '=', $user->id);
            $channels = $channels->filter(fn (Channel $ch) => $ch->user_id !== $user->id);
            if ($managedChannel !== null) {
                $channels = $channels->prepend($managedChannel);
            }
        }
        $pagesCount = ceil(count($channels) / $pageSize);
        if ($page > $pagesCount) {
            $page = $pagesCount;
        }
        $channels = $channels->forPage($page, $pageSize);
        $result = new ChannelsListViewModel();
        $result->pagesCount = $pagesCount;
        $result->channels = [];
        foreach ($channels as $channel) {
            $photos = MediaFilesHelper::loadImagesFromDisk(json_decode($channel->photo_paths));
            $result->channels[] = new ChannelListViewModel(
                $channel->id,
                $channel->name,
                $channel->description,
                $photos
            );
        }
        return $result;
    }

    /**@return ChannelSelectViewModel[] */
    public function getChannelsListForSelect(): array
    {
        return Channel::query()
            ->orderBy('name')
            ->get()
            ->map(fn (Channel $ch) => new ChannelSelectViewModel($ch->id, $ch->name))
            ->toArray();
    }
}
