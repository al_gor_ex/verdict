<?php

namespace App\Mail;

use App\Enums\ApplicationStatus;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplicationViewed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        public int $applicationId,
        public ApplicationStatus $newStatus,
        public string $projectName,
        public string $channelName
    )
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "Заявка #{$this->applicationId} ";
        $subject .= ($this->newStatus == ApplicationStatus::APPROVED()) ? 'одобрена!' : 'отклонена';
        return $this
            ->subject($subject)
            ->markdown('emails.application-viewed');
    }
}
