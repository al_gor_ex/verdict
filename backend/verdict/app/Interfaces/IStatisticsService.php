<?php


namespace App\Interfaces;


use App\Http\Dto\View\StatisticsViewModel;

interface IStatisticsService
{
    public function getStatsForCharts(): StatisticsViewModel;
}
