<?php


namespace App\Interfaces;


interface ICityService
{
    /** @return string[] */
    public function getCitiesListForSelect(): array;
}
