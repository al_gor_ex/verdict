<?php


namespace App\Interfaces;


use App\Http\Dto\Binding\ProjectBindingModel;
use App\Http\Dto\Binding\ProjectsListFiltersBindingModel;
use App\Http\Dto\View\ProjectEditorViewModel;
use App\Http\Dto\View\ProjectsListViewModel;
use App\Models\User;

interface IProjectService
{
    public function createProject(User $user, ProjectBindingModel $model): void;

    public function getProjectsList(?User $user, ProjectsListFiltersBindingModel $filters): ProjectsListViewModel;

    public function getProjectForEditing(int $id): ProjectEditorViewModel;

    public function updateProject(User $user, ProjectBindingModel $model);
}
