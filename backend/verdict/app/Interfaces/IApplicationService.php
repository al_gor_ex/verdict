<?php

namespace App\Interfaces;

use App\Http\Dto\Binding\ApplicationBindingModel;
use App\Http\Dto\Binding\ApplicationsListFiltersBindingModel;
use App\Http\Dto\View\ApplicationsListViewModel;
use App\Http\Dto\View\VideoViewModel;
use App\Models\User;

interface IApplicationService
{
    /**
     * @return int Id of the created application
     */
    public function createApplication(User $user, ApplicationBindingModel $model): int;

    public function getApplicationsList(
        User $user,
        ApplicationsListFiltersBindingModel $filters
    ): ApplicationsListViewModel;

    public function getApplicationVideo(User $user, string $id): VideoViewModel;

    public function cancelApplication(User $user, int $id): void;

    public function approveApplication(User $user, int $id): void;

    public function declineApplication(User $user, int $id): void;

    public function hasAlreadyApplied(int $projectId, int $participantId): bool;

    public function isProjectAvailable(int $projectId): bool;

    public function anyFreePlaces(int $projectId): bool;

    public function declineAllPendingApplications(User $user, int $projectId): void;
}
