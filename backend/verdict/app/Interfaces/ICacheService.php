<?php


namespace App\Interfaces;


use App\Exceptions\CacheMissException;
use App\Http\Dto\View\ChannelSelectViewModel;
use App\Http\Dto\View\StatisticsViewModel;

interface ICacheService
{
    /**
     * @throws CacheMissException
     * @return ChannelSelectViewModel[]
     */
    public function getChannelsListForSelect(): array;

    /**
     * @throws CacheMissException
     * @return string[]
     */
    public function getCitiesListForSelect(): array;

    /**
     * @throws CacheMissException
     */
    public function getStatsForCharts(): StatisticsViewModel;

    /**@param ChannelSelectViewModel[]|null $freshData */
    public function syncChannelsListForSelect(?array $freshData): void;

    /**@param string[]|null $freshData */
    public function syncCitiesListForSelect(?array $freshData): void;

    public function syncStatsForCharts(?StatisticsViewModel $freshData): void;
}
