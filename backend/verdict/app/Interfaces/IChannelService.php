<?php


namespace App\Interfaces;


use App\Http\Dto\View\ChannelSelectViewModel;
use App\Http\Dto\View\ChannelsListViewModel;
use App\Models\User;

interface IChannelService
{
    public function getChannelsList(?User $user, int $page, int $pageSize, ?string $searchName = null): ChannelsListViewModel;

    /**@return ChannelSelectViewModel[] */
    public function getChannelsListForSelect(): array;
}
