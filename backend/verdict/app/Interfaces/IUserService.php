<?php


namespace App\Interfaces;


use App\Http\Dto\View\UserViewModel;
use App\Models\User;

interface IUserService
{
    public function getUserInfo(User $user): UserViewModel;
}
