<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $photo_paths
 * @property string $description
 * @property string $city
 * @property int $participants_limit
 * @property int|null $fee
 * @property string $starts_at
 * @property int $channel_id
 * @property-read Collection|Application[] $applications
 * @property-read Channel $channel
 */
class Project extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }
}
