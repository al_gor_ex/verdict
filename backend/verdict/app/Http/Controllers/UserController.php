<?php

namespace App\Http\Controllers;

use App\Interfaces\IUserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(
        private IUserService $userService
    )
    {
    }

    public function show(Request $request)
    {
        return response()->json(
            $this->userService->getUserInfo($request->user())
        );
    }
}
