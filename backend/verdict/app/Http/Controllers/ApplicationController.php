<?php

namespace App\Http\Controllers;

use App\Enums\ApplicationSortingField;
use App\Enums\SortingDirection;
use App\Http\Dto\Binding\ApplicationBindingModel;
use App\Http\Dto\Binding\ApplicationsListFiltersBindingModel;
use App\Interfaces\IApplicationService;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function __construct(
        private IApplicationService $applicationService
    )
    {
    }

    public function index(Request $request)
    {
        $sortingParams = explode(':', $request->query('sort'));
        $sortingField = new ApplicationSortingField($sortingParams[0]);
        $sortingDirection = new SortingDirection($sortingParams[1]);
        $applications = $this->applicationService->getApplicationsList(
            $request->user(),
            new ApplicationsListFiltersBindingModel(
                $sortingField,
                $sortingDirection,
                $request->query('onlyPending')
            )
        );
        return response()->json($applications);
    }

    public function video(Request $request, string $id)
    {
        return response()->json(
            $this->applicationService->getApplicationVideo($request->user(), $id),
            200,
            [],
            JSON_UNESCAPED_SLASHES
        );
    }

    public function store(Request $request)
    {
        $newId = $this->applicationService->createApplication(
            $request->user(),
            new ApplicationBindingModel(
                $request->input('projectId'),
                $request->input('description'),
                $request->input('video')
            )
        );
        return response()->json([
            'newId' => $newId
        ], 201);
    }

    public function cancel(Request $request, int $id)
    {
        $this->applicationService->cancelApplication($request->user(), $id);
        return response()->json();
    }

    public function approve(Request $request, int $id)
    {
        $this->applicationService->approveApplication($request->user(), $id);
        return response()->json();
    }

    public function decline(Request $request, int $id)
    {
        $this->applicationService->declineApplication($request->user(), $id);
        return response()->json();
    }
}
