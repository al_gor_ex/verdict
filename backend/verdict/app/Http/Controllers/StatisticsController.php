<?php

namespace App\Http\Controllers;

use App\Exceptions\CacheMissException;
use App\Interfaces\ICacheService;
use App\Interfaces\IStatisticsService;

class StatisticsController extends Controller
{
    public function __construct(
        private IStatisticsService $statisticsService,
        private ICacheService $cacheService
    )
    {
    }

    public function stats()
    {
        try {
            $stats = $this->cacheService->getStatsForCharts();
        } catch (CacheMissException) {
            $stats = $this->statisticsService->getStatsForCharts();
            $this->cacheService->syncStatsForCharts($stats);
        }
        return response()->json(
            $stats
        );
    }
}
