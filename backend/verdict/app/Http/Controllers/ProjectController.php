<?php

namespace App\Http\Controllers;

use App\Http\Dto\Binding\ProjectBindingModel;
use App\Http\Dto\Binding\ProjectsListFiltersBindingModel;
use App\Interfaces\IProjectService;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function __construct(
        private IProjectService $projectService
    )
    {
    }

    public function index(Request $request)
    {
        return response()->json(
            $this->projectService->getProjectsList(
                $request->user(),
                new ProjectsListFiltersBindingModel(
                    $request->query('page'),
                    $request->query('pageSize'),
                    $request->query('searchName'),
                    $request->query('channelId'),
                    $request->query('city'),
                    $request->query('onlyAvailable'),
                    $request->query('onlyFee')
                )
            )
        );
    }

    public function store(Request $request)
    {
        $this->projectService->createProject(
            $request->user(),
            new ProjectBindingModel(
                null,
                $request->input('name'),
                $request->input('photos'),
                $request->input('description'),
                $request->input('city'),
                $request->input('participantsLimit'),
                $request->input('fee'),
                $request->input('startsAt')
            )
        );
        return response('', 201);
    }

    public function edit(int $id)
    {
        return response()->json(
            $this->projectService->getProjectForEditing($id)
        );
    }

    public function update(Request $request, int $id)
    {
        $this->projectService->updateProject(
            $request->user(),
            new ProjectBindingModel(
                $id,
                $request->input('name'),
                $request->input('photos'),
                $request->input('description'),
                $request->input('city'),
                $request->input('participantsLimit'),
                $request->input('fee'),
                $request->input('startsAt')
            )
        );
        return response('', 200);
    }

}
