<?php

namespace App\Http\Controllers;

use App\Exceptions\CacheMissException;
use App\Interfaces\ICacheService;
use App\Interfaces\ICityService;

class CityController extends Controller
{
    public function __construct(
        private ICityService $cityService,
        private ICacheService $cacheService
    )
    {
    }

    public function select()
    {
        try {
            $cities = $this->cacheService->getCitiesListForSelect();
        } catch (CacheMissException) {
            $cities = $this->cityService->getCitiesListForSelect();
            $this->cacheService->syncCitiesListForSelect($cities);
        }
        return response()->json(
            $cities
        );
    }
}
