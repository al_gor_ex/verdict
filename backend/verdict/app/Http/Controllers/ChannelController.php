<?php

namespace App\Http\Controllers;

use App\Exceptions\CacheMissException;
use App\Interfaces\ICacheService;
use App\Interfaces\IChannelService;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    public function __construct(
        private IChannelService $channelService,
        private ICacheService $cacheService
    )
    {
    }

    public function index(Request $request)
    {
        return response()->json(
            $this->channelService->getChannelsList(
                $request->user(),
                $request->query('page'),
                $request->query('pageSize'),
                $request->query('searchName')
            )
        );
    }

    public function select()
    {
        try {
            $channels = $this->cacheService->getChannelsListForSelect();
        } catch (CacheMissException) {
            $channels = $this->channelService->getChannelsListForSelect();
            $this->cacheService->syncChannelsListForSelect($channels);
        }
        return response()->json(
            $channels
        );
    }
}
