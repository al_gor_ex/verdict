<?php


namespace App\Http\Dto\View;


class StatisticsViewModel
{
    /**
     * @param ApplicationsCountViewModel[] $newApplicationsPerDay
     * @param ChannelPopularityViewModel[] $mostPopularChannels
     */
    public function __construct(
        public array $newApplicationsPerDay,
        public array $mostPopularChannels
    )
    {
    }
}
