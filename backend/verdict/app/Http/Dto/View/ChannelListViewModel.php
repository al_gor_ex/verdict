<?php


namespace App\Http\Dto\View;


class ChannelListViewModel
{

    /** @param string[] $photos */
    public function __construct(
        public int $id,
        public string $name,
        public string $description,
        public array $photos
    )
    {
    }
}
