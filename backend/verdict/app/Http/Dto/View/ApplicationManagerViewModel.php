<?php


namespace App\Http\Dto\View;


class ApplicationManagerViewModel
{
    public function __construct(
        public string $name,
        public string $channelName,
        public string $photo
    )
    {
    }
}
