<?php


namespace App\Http\Dto\View;


class ApplicationsCountViewModel
{
    public function __construct(
        public string $date,
        public int $count
    )
    {
    }
}
