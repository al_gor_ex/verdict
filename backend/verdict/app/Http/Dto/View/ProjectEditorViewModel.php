<?php


namespace App\Http\Dto\View;


class ProjectEditorViewModel
{
    public function __construct(
        public string $name,
        public string $description,
        public string $city,
        public int $approvedParticipantsCount,
        public int $participantsLimit,
        public ?int $fee,
        public string $startsAt
    )
    {
    }
}
