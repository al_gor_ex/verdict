<?php


namespace App\Http\Dto\View;


class UserViewModel
{
    public function __construct(
        public string $name,
        public string $role,
        public string $photo
    )
    {
    }
}
