<?php


namespace App\Http\Dto\View;


class ChannelSelectViewModel
{
    public function __construct(
        public int $id,
        public string $name
    )
    {
    }
}
