<?php


namespace App\Http\Dto\View;


class ApplicationListViewModel
{
    public function __construct(
        public int $id,
        public string $createdAt,
        public string $status,
        public string $projectName,
        public string $description,
        public string $videoId,
        public ?ApplicationManagerViewModel $manager,
        public ?ApplicationParticipantViewModel $participant
    )
    {
    }
}
