<?php


namespace App\Http\Dto\View;


class ChannelsListViewModel
{
    public int $pagesCount;
    /**@var ChannelListViewModel[]*/
    public array $channels;
}
