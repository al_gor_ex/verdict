<?php


namespace App\Http\Dto\View;


class ChannelPopularityViewModel
{
    public function __construct(
        public string $name,
        public int $percent
    )
    {
    }
}
