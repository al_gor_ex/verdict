<?php


namespace App\Http\Dto\View;


class ProjectsListViewModel
{
    public int $pagesCount;
    /** @var ProjectListViewModel[] */
    public array $projects = [];
}
