<?php


namespace App\Http\Dto\View;


class VideoViewModel
{
    public function __construct(
        public string $video
    )
    {
    }
}
