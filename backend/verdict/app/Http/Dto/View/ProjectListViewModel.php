<?php


namespace App\Http\Dto\View;


class ProjectListViewModel
{
    /** @param string[] $photos */
    public function __construct(
        public int $id,
        public string $name,
        public string $description,
        public array $photos,
        public string $channelName,
        public string $city,
        public string $startsAt,
        public ProjectParticipantsCountModel $participantsCount,
        public ?int $fee = null,
        public ?bool $isAvailable = null,
        public ?bool $hasApplied = null,
        public ?bool $canEdit = null
    )
    {
    }
}
