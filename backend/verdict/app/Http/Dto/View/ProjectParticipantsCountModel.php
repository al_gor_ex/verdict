<?php


namespace App\Http\Dto\View;


class ProjectParticipantsCountModel
{
    public function __construct(
        public int $approved,
        public int $limit
    )
    {
    }
}
