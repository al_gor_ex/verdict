<?php


namespace App\Http\Dto\View;


class ApplicationParticipantViewModel
{
    public function __construct(
        public string $name,
        public string $email,
        public string $gender,
        public int $age,
        public string $city,
        public string $photo
    )
    {
    }
}
