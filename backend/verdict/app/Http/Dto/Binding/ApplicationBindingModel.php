<?php


namespace App\Http\Dto\Binding;


class ApplicationBindingModel
{
    public function __construct(
        public int $projectId,
        public string $description,
        public string $video
    )
    {
    }
}
