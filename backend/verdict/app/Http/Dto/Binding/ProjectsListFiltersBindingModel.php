<?php


namespace App\Http\Dto\Binding;


class ProjectsListFiltersBindingModel
{
    public function __construct(
        public int $page,
        public int $pageSize,
        public ?string $searchName = null,
        public ?int $channelId = null,
        public ?string $city = null,
        public ?bool $onlyAvailable = null,
        public ?bool $onlyFee = null
    )
    {
    }
}
