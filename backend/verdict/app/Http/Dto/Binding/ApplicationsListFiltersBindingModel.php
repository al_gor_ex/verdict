<?php


namespace App\Http\Dto\Binding;


use App\Enums\ApplicationSortingField;
use App\Enums\SortingDirection;

class ApplicationsListFiltersBindingModel
{
    public function __construct(
        public ApplicationSortingField $sortingField,
        public SortingDirection $sortingDirection,
        public ?bool $onlyPending
    )
    {
    }
}
