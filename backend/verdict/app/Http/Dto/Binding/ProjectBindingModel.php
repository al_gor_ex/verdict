<?php


namespace App\Http\Dto\Binding;


class ProjectBindingModel
{
    /**
     * @param string[] $photos
     */
    public function __construct(
        public ?int $id,
        public string $name,
        public array $photos,
        public string $description,
        public string $city,
        public int $participantsLimit,
        public ?int $fee,
        public string $startsAt
    )
    {
    }
}
