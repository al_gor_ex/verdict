<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next (\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param string $role
     */
    public function handle(Request $request, Closure $next, string $role)
    {
        if ($request->user()->role === $role) {
            return $next($request);
        } else {
            Log::info('Access denied (wrong user role)', [
                'userId' => $request->user()->id,
                'userIp' => $request->ip(),
                'userAgent' => $request->userAgent(),
                'requestedUrl' => $request->fullUrl(),
                'userRole' => $request->user()->role,
                'requiredRole' => $role
            ]);
            return response()->json(
                [
                    'error' => [
                        'message' => "У Вас нет прав доступа к запрашиваемому ресурсу",
                        'details' => 'Access denied (wrong user role)'
                    ]
                ],
                403
            );
        }
    }
}
