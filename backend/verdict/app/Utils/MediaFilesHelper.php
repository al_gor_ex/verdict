<?php


namespace App\Utils;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;

class MediaFilesHelper
{
    /**
     * @param string $baseCode Base64-encoded image (.png or .jpeg)
     * @param string $folder Storage folder (must end with a slash)
     * @return string New image's path in local filesystem
     */
    public static function saveImageOnDisk(string $baseCode, string $folder, int $width, int $height): string
    {
        $baseParts = explode(',', $baseCode);
        $imageCode = $baseParts[1];
        $imageExtension = (Str::of($baseParts[0])->contains('png')) ? '.png' : '.jpeg';
        $dataToWrite = base64_decode($imageCode);
        $newFilePathInStorage = $folder .
            Str::random(32) .
            Carbon::now()->isoFormat('MM_DD_HH_mm') .
            $imageExtension;
        Storage::put($newFilePathInStorage, $dataToWrite);
        static::resizeImage($newFilePathInStorage, $width, $height);
        Log::info('Saved new image to local storage', [
            'path' => $newFilePathInStorage,
            'size' => Storage::size($newFilePathInStorage)
        ]);
        return $newFilePathInStorage;
    }

    /**
     * @param string $baseCode Base64-encoded video (.mp4)
     * @param string $folder Storage folder (must end with a slash)
     * @return string Name of video file saved to the specified $folder without extension
     */
    public static function saveVideoOnDisk(string $baseCode, string $folder): string
    {
        $videoCode = explode(',', $baseCode)[1];
        $dataToWrite = base64_decode($videoCode);
        $newFileName = Str::random(32);
        $newFilePath = $folder . $newFileName . '.mp4';
        Storage::put($newFilePath, $dataToWrite);
        Log::info('Saved new video to local storage', [
            'path' => $newFilePath,
            'size' => Storage::size($newFilePath)
        ]);
        return $newFileName;
    }

    /**
     * @param string $path Image file location relative to local storage
     * @return string Base64-encoded image
     */
    public static function loadImageFromDisk(string $path): string
    {
        $baseCode = base64_encode(Storage::get($path));
        $mimeType = Storage::mimeType($path);
        $baseHeader = "data:$mimeType;base64,";
        return $baseHeader . $baseCode;
    }

    /**
     * @param string[] $paths Image files locations relative to local storage
     * @return string[] Array of base64-encoded image
     */
    public static function loadImagesFromDisk(array $paths): array
    {
        return collect($paths)
            ->map(fn (string $path) => static::loadImageFromDisk($path))
            ->toArray();
    }

    /**
     * @param string $folder Storage folder (must end with a slash)
     * @param string $fileName Name of video file in the specified $folder without extension
     * @return string Base64-encoded video (.mp4)
     */
    public static function loadVideoFromDisk(string $folder, string $fileName): string
    {
        $baseHeader = "data:video/mp4;base64,";
        $baseCode = base64_encode(Storage::get($folder . $fileName . '.mp4'));
        return $baseHeader . $baseCode;
    }

    /**
     * @param array $paths Image files locations relative to local storage
     */
    public static function deleteImagesFromDisk(array $paths): void
    {
        Storage::delete($paths);
        Log::info('Deleted images from local storage', [
            'deletedPaths' => $paths
        ]);
    }

    private static function resizeImage(string $path, int $newWidth, int $newHeight): void
    {
        ImageManagerStatic::make(Storage::path($path))
            ->resize($newWidth, $newHeight)
            ->save();
    }
}
