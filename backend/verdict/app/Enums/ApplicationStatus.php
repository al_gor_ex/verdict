<?php


namespace App\Enums;


use MyCLabs\Enum\Enum;

/**
 * @method static ApplicationStatus PENDING()
 * @method static ApplicationStatus CANCELED()
 * @method static ApplicationStatus APPROVED()
 * @method static ApplicationStatus DECLINED()
 */
class ApplicationStatus extends Enum
{
    private const PENDING = 'pending';
    private const CANCELED = 'canceled';
    private const APPROVED = 'approved';
    private const DECLINED = 'declined';
}
