<?php

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * @method static Role PARTICIPANT()
 * @method static Role MANAGER()
 * @method static Role ADMIN()
 */
class Role extends Enum
{
    private const PARTICIPANT = 'participant';
    private const MANAGER = 'manager';
    private const ADMIN = 'admin';
}
