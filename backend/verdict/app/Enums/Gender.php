<?php


namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * @method static Gender MALE()
 * @method static Gender FEMALE()
 */
class Gender extends Enum
{
    private const MALE = 'male';
    private const FEMALE = 'female';
}
