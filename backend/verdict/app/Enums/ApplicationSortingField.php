<?php


namespace App\Enums;


use MyCLabs\Enum\Enum;

/**
 * @method static ApplicationSortingField CREATED_AT()
 * @method static ApplicationSortingField PARTICIPANT_NAME()
 * @method static ApplicationSortingField PROJECT_NAME()
 * @method static ApplicationSortingField STATUS()
 */
class ApplicationSortingField extends Enum
{
    private const CREATED_AT = 'created_at';
    private const PARTICIPANT_NAME = 'participant_name';
    private const PROJECT_NAME = 'project_name';
    private const STATUS = 'status';
}
