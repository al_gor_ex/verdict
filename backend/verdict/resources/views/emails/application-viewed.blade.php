@if($newStatus == 'approved')
Поздравляем!
<br><br>
@endif
Ваша заявка номер #{{$applicationId}} на проект "{{$projectName}}" телеканала "{{$channelName}}" 
@if($newStatus == 'approved')
одобрена 
@else
отклонена 
@endif
представителем проекта
@if($newStatus == 'approved')
!<br><br>Дальнейшие инструкции будут отправлены представителем проекта на этот же адрес электронной почты.
@else
.
@endif
<br><br>
@if($newStatus == 'approved')
Желаем удачи на проекте и ждём Вас снова на платформе кастингов Verdict
@else
В следующий раз всё обязательно получится! Попробуйте принять участие в других проектах на платформе кастингов Verdict
@endif
