<?php

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\StatisticsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/v1')->group(function() {
    Route::middleware('auth:sanctum')->group(function() {
        Route::get('/user', [UserController::class, 'show']);
    });

    Route::get('/channels', [ChannelController::class, 'index']);

    Route::prefix('/projects')->group(function() {
        Route::get('/', [ProjectController::class, 'index']);
        Route::middleware(['auth:sanctum', 'role:manager'])->group(function() {
            Route::post('/', [ProjectController::class, 'store']);
            Route::get('/{id}/edit', [ProjectController::class, 'edit']);
            Route::put('/{id}', [ProjectController::class, 'update']);
        });
    });

    Route::prefix('/select')->group(function() {
        Route::get('/cities', [CityController::class, 'select']);
        Route::get('/channels', [ChannelController::class, 'select']);
    });

    Route::prefix('/applications')->middleware('auth:sanctum')->group(function() {
        Route::get('/', [ApplicationController::class, 'index']);
        Route::middleware('role:participant')->group(function() {
            Route::post('/', [ApplicationController::class, 'store']);
            Route::post('/{id}/cancel', [ApplicationController::class, 'cancel']);
        });
        Route::middleware('role:manager')->group(function() {
            Route::post('/{id}/approve', [ApplicationController::class, 'approve']);
            Route::post('/{id}/decline', [ApplicationController::class, 'decline']);
        });
    });

    Route::middleware('auth:sanctum')->group(function() {
        Route::get('/videos/{id}', [ApplicationController::class, 'video']);
    });

    Route::middleware(['auth:sanctum', 'role:admin'])->group(function() {
        Route::get('/stats', [StatisticsController::class, 'stats']);
    });
});
