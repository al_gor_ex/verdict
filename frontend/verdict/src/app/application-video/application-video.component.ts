import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApplicationService} from "../services/application.service";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";

@Component({
  selector: 'app-application-video',
  templateUrl: './application-video.component.html',
  styleUrls: ['./application-video.component.css']
})
export class ApplicationVideoComponent implements OnInit {

  isVideoLoading: boolean = true
  video: string = ''
  @ViewChild('errorSwal')
  errorSwal: SwalComponent
  private videoId: string = ''

  constructor(
    private route: ActivatedRoute,
    private applicationService: ApplicationService,
    private router: Router
  ) { }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.videoId = this.route.snapshot.paramMap.get('id')!
    this.loadVideo()
  }

  redirectToApplications(): void {
    this.router.navigateByUrl('/applications')
  }

  private loadVideo(): void {
    this.applicationService.getApplicationVideo(this.videoId).subscribe({
      next: (result) => {
        this.video = result.video
        this.isVideoLoading = false
      },
      error: () => {
        this.errorSwal.fire()
        this.isVideoLoading = false
      }
    })
  }

}
