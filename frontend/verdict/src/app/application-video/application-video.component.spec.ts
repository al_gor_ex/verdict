import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationVideoComponent } from './application-video.component';

describe('ApplicationVideoComponent', () => {
  let component: ApplicationVideoComponent;
  let fixture: ComponentFixture<ApplicationVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplicationVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
