import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {ApplicationService} from "../services/application.service";
import {ApplicationBindingModel} from "../services/dto/binding/application-binding-model";

@Component({
  selector: 'app-new-application',
  templateUrl: './new-application.component.html',
  styleUrls: ['./new-application.component.css']
})
export class NewApplicationComponent implements OnInit {

  projectId: number = 0
  projectName: string = ''
  editor = ClassicEditor
  @ViewChild('applicationDescriptionEditor') applicationDescriptionEditor: CKEditorComponent | undefined;
  applicationDescriptionEditorConfig: object = {
    toolbar: {
      items: [
        'undo',
        'redo',
        '|',
        'bold',
        'italic',
        '|',
        'numberedList',
        'bulletedList',
        '|',
        'link'
      ]
    },
    placeholder: 'Почему Вы решили принять участие в проекте, расскажите о своих талантах, каких целей хотите добиться, участвуя в проекте'
  }
  description: string = ''
  video: string = ''
  formErrors = {
    description: false,
    video: false
  }
  isBtnSubmitVisible: boolean = true
  createdApplicationId: number = 0
  @ViewChild('validationErrorSwal')
  validationErrorSwal: SwalComponent
  @ViewChild('errorSwal')
  errorSwal: SwalComponent
  @ViewChild('successSwal')
  successSwal: SwalComponent

  constructor(
    public router: Router,
    private cookieService: CookieService,
    private applicationService: ApplicationService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.getChosenProjectInfo()
  }

  getChosenProjectInfo(): void {
    this.projectId = Number(this.cookieService.get('application-project-id'))
    this.projectName = this.cookieService.get('application-project-name')
    // Clear project cookies
    this.cookieService.delete('application-project-id')
    this.cookieService.delete('application-project-name')
  }

  onSubmit(): void {
    this.isBtnSubmitVisible = false
    this.validateApplicationForm()
    if (this.anyFormErrors()) {
      this.validationErrorSwal.fire()
      this.isBtnSubmitVisible = true
      return
    }
    let application = new ApplicationBindingModel()
    application.projectId = this.projectId
    application.description = this.description
    application.video = this.video
    this.applicationService.createApplication(application).subscribe({
      next: (result) => {
        this.createdApplicationId = result.newId
        this.changeDetectorRef.detectChanges()
        this.successSwal.fire()
        this.router.navigateByUrl('/applications')
      },
      error: (error) => {
        this.errorSwal.fire()
        this.isBtnSubmitVisible = true
      }
    })
  }

  validateApplicationForm(): void {
    this.validateDescription()
    this.validateVideo()
  }

  anyFormErrors(): boolean {
    return Object.values(this.formErrors).includes(true)
  }

  validateDescription(): void {
    this.formErrors.description = (this.description.length > 2_000 || this.description == '')
  }

  validateVideo(): void {
    let isValid = true
    let videoInput = document.getElementById('video') as HTMLInputElement
    if (videoInput.files!.length == 0) {
      isValid = false
    } else {
      let videoFile = videoInput.files!.item(0)!
      if (videoFile.type !== 'video/mp4' || videoFile.size > 100 * 1_048_576) {
        isValid = false
      }
    }
    this.formErrors.video = !isValid
  }

  extractVideoBase64(): void {
    let reader = new FileReader()
    reader.onloadend = () => {
      this.video = reader.result as string
    }
    let input = document.getElementById('video') as HTMLInputElement
    let file: File = input.files!.item(0)!
    reader.readAsDataURL(file)
  }
}
