import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewEditProjectComponent } from './new-edit-project.component';

describe('NewEditProjectComponent', () => {
  let component: NewEditProjectComponent;
  let fixture: ComponentFixture<NewEditProjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewEditProjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewEditProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
