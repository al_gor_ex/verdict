import {Component, OnInit, ViewChild} from '@angular/core';
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../services/project.service";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";
import {ProjectBindingModel} from "../services/dto/binding/project-binding-model";

@Component({
  selector: 'app-new-edit-project',
  templateUrl: './new-edit-project.component.html',
  styleUrls: ['./new-edit-project.component.css']
})
export class NewEditProjectComponent implements OnInit {

  projectId: number = -1
  name: string = ''
  photos: string[] = []
  description: string = ''
  editor = ClassicEditor
  @ViewChild('projectDescriptionEditor') projectDescriptionEditor: CKEditorComponent | undefined;
  projectDescriptionEditorConfig: object = {
    toolbar: {
      items: [
        'undo',
        'redo',
        '|',
        'bold',
        'italic',
        '|',
        'numberedList',
        'bulletedList',
        '|',
        'link'
      ]
    },
    placeholder: 'Цели проекта, какие таланты ожидаете от участников, что проект может дать участникам, информация об организаторах и жюри'
  }
  city: string = ''
  participantsLimit: number = 1
  minParticipantsCount: number | null = null
  isFeeEnabled = false
  fee: number | null = null
  startsAt: string = ''
  isBtnSubmitVisible: boolean = true
  @ViewChild('validationErrorSwal')
  validationErrorSwal: SwalComponent
  @ViewChild('successSwal')
  successSwal: SwalComponent
  formErrors = {
    name: false,
    photos: false,
    description: false,
    city: false,
    startsAt: false
  }

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    private projectService: ProjectService
  ) {
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.projectId = Number(this.route.snapshot.paramMap.get('id'));
    if (this.projectId != -1) {
      this.loadProjectData()
    }
  }

  toggleFee(): void {
    this.fee = (this.isFeeEnabled) ? 1 : null
  }

  onSubmit(): void {
    this.isBtnSubmitVisible = false
    this.validateProjectForm()
    if (this.anyFormErrors()) {
      this.validationErrorSwal.fire()
      this.isBtnSubmitVisible = true
      return
    }
    let project = new ProjectBindingModel()
    project.name = this.name
    project.photos = this.photos
    project.description = this.description
    project.city = this.city
    project.participantsLimit = this.participantsLimit
    project.fee = this.fee
    project.startsAt = this.startsAt
    let requestObservable = (this.projectId == -1) ?
      this.projectService.createProject(project) :
      this.projectService.updateProject(this.projectId, project)
    requestObservable.subscribe({
      next: () => {
        this.successSwal.fire()
        this.router.navigateByUrl('/projects')
      }
    })
  }

  validateName(): void {
    this.formErrors.name = (this.name === '' || this.name.length > 50)
  }

  validatePhotos(): void {
    let isValid = true
    let photoInput = document.getElementById('photos') as HTMLInputElement
    if (photoInput.files!.length > 3 || (this.projectId == -1 && photoInput.files!.length == 0)) {
      isValid = false
    } else {
      for (let i = 0; i < photoInput.files!.length; i++) {
        if (!this.isPhotoValid(
          photoInput.files!.item(i)!,
          ['image/jpeg', 'image/png'],
          5
        )) {
          isValid = false
          break
        }
      }
    }
    this.formErrors.photos = !isValid
  }

  validateDescription(): void {
    this.formErrors.description = (this.description.length > 2_000 || this.description == '')
  }

  validateCity(): void {
    this.formErrors.city = (this.city == '' || this.city.length > 50)
  }

  validateStartDate(): void {
    let today = new Date()
    today.setHours(0, 0, 0, 0)
    let pickedDate = new Date(this.startsAt)
    pickedDate.setHours(0, 0, 0, 0)
    this.formErrors.startsAt = (this.startsAt == '' || pickedDate <= today)
  }

  extractInputFilesBase64(): void {
    let input = document.getElementById('photos') as HTMLInputElement
    this.photos = []
    for (let i = 0; i < input.files!.length; i++) {
      let file: File = input.files!.item(i)!
      let reader = new FileReader()
      reader.onloadend = () => {
        let fileContent = reader.result as string
        this.photos.push(fileContent)
      }
      reader.readAsDataURL(file)
    }
  }

  private validateProjectForm(): void {
    this.validateName()
    this.validatePhotos()
    this.validateDescription()
    this.validateCity()
    this.validateStartDate()
  }

  private anyFormErrors(): boolean {
    return Object.values(this.formErrors).includes(true)
  }

  private loadProjectData(): void {
    this.projectService.getProjectForEditing(this.projectId).subscribe({
      next: (project) => {
        this.name = project.name
        this.description = project.description
        this.city = project.city
        this.minParticipantsCount = (project.approvedParticipantsCount > 0) ? project.approvedParticipantsCount : null
        this.participantsLimit = project.participantsLimit
        this.fee = project.fee
        if (this.fee !== null) {
          this.isFeeEnabled = true
        }
        this.startsAt = project.startsAt
      }
    })
  }

  private isPhotoValid(photo: File, mimeTypes: string[], maxSizeMb: number): boolean {
    return !(
      !mimeTypes.includes(photo.type) ||
      photo.size > maxSizeMb * 1_048_576
    )
  }

}
