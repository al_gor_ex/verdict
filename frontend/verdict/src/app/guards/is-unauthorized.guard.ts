import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from "../services/user.service";
import {Role} from "../services/enums/role";

@Injectable({
  providedIn: 'root'
})
export class IsUnauthorizedGuard implements CanActivate {

  constructor(
    private userService: UserService,
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let role = this.userService.getCurrentUserRole()
    if (role === null) {
      return true
    } else {
      this.router.navigateByUrl((role === Role.Admin) ? '/admin-panel' : '/applications')
      return false
    }
  }

}
