import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {NotFoundComponent} from "./not-found/not-found.component";
import {MainComponent} from "./main/main.component";
import {ChannelsComponent} from "./channels/channels.component";
import {ProjectsComponent} from "./projects/projects.component";
import {ApplicationsComponent} from "./applications/applications.component";
import {ApplicationVideoComponent} from "./application-video/application-video.component";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {NewApplicationComponent} from "./new-application/new-application.component";
import {NewEditProjectComponent} from "./new-edit-project/new-edit-project.component";
import {AdminPanelComponent} from "./admin-panel/admin-panel.component";
import {IsAuthorizedGuard} from "./guards/is-authorized.guard";
import {IsUnauthorizedGuard} from "./guards/is-unauthorized.guard";
import {IsParticipantGuard} from "./guards/is-participant.guard";
import {IsManagerGuard} from "./guards/is-manager.guard";
import {IsAdminGuard} from "./guards/is-admin.guard";

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: 'channels',
    component: ChannelsComponent
  },
  {
    path: 'projects',
    component: ProjectsComponent
  },
  {
    path: 'applications',
    component: ApplicationsComponent,
    canActivate: [IsAuthorizedGuard]
  },
  {
    path: 'application-video/:id',
    component: ApplicationVideoComponent,
    canActivate: [IsAuthorizedGuard]
  },
  {
    path: 'registration',
    component: RegistrationComponent,
    canActivate: [IsUnauthorizedGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsUnauthorizedGuard]
  },
  {
    path: 'new-application',
    component: NewApplicationComponent,
    canActivate: [IsParticipantGuard]
  },
  {
    path: 'new-edit-project/:id',
    component: NewEditProjectComponent,
    canActivate: [IsManagerGuard]
  },
  {
    path: 'admin-panel',
    component: AdminPanelComponent,
    canActivate: [IsAdminGuard]
  },
  {
    path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
