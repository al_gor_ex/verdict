import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  currentPage: number = 1
  totalPages: number = 1
  @Output()
  pageChangedEvent: EventEmitter<void> = new EventEmitter<void>()

  constructor() { }

  ngOnInit(): void {
  }

  increasePage(): void {
    if (this.currentPage < this.totalPages) {
      this.currentPage++
      this.pageChangedEvent.emit()
    }
  }

  decreasePage(): void {
    if (this.currentPage > 1) {
      this.currentPage--
      this.pageChangedEvent.emit()
    }
  }

}
