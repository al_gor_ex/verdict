import {Component, OnInit, ViewChild} from '@angular/core';
import {ProjectService} from "../services/project.service";
import {ChannelService} from "../services/channel.service";
import {PaginationComponent} from "../pagination/pagination.component";
import {Router} from "@angular/router";
import {debounceTime, Subject} from "rxjs";
import {Role} from "../services/enums/role";
import {UserService} from "../services/user.service";
import {ProjectsQueryBindingModel} from "../services/dto/binding/projects-query-binding-model";
import {ProjectsListViewModel} from "../services/dto/view/projects-list-view-model";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {


  searchNameInputText: string = ''
  searchNameInputEvent = new Subject<void>()
  searchName: string = ''
  isChannelsSelectLoading: boolean = false
  selectedChannelId: number | null = null
  channels: { id: number, name: string }[] = []
  isCitiesSelectLoading: boolean = false
  selectedCityName: string | null = null
  cities: {name: string}[] = []
  onlyAvailable: boolean = false
  onlyFee: boolean = false
  role = Role
  currentRole: Role | null = null
  isCatalogLoading: boolean = false
  projectsCatalog: ProjectsListViewModel
  @ViewChild('paginator')
  paginator: PaginationComponent
  pageSize: number = 2

  constructor(
    public router: Router,
    private channelService: ChannelService,
    private projectService: ProjectService,
    private userService: UserService,
    private cookieService: CookieService
  ) {
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.currentRole = this.userService.getCurrentUserRole()
  }

  ngAfterViewInit(): void {
    this.synchronizeComponentWithUrl()
    this.synchronizeUrlWithComponent()
    this.loadChannelsList()
    this.loadCitiesList()
    this.refreshCatalog()
    this.paginator.pageChangedEvent.subscribe(() => {
      this.synchronizeUrlWithComponent()
      this.refreshCatalog()
    })
    this.searchNameInputEvent
      .pipe(debounceTime(500))
      .subscribe({
        next: () => {
          this.searchName = this.searchNameInputText
          this.onFiltersChange()
        }
      })
  }

  onFiltersChange(): void {
    this.synchronizeUrlWithComponent()
    this.refreshCatalog()
  }

  redirectToProjectEditor(id: number): void {
    this.router.navigateByUrl(`/new-edit-project/${id}`)
  }

  onBtnCreateApplicationClick(projectId: number): void {
    let url = (this.currentRole == null) ? '/registration' : '/new-application'
    if (this.currentRole == this.role.Participant) {
      this.cookieService.set('application-project-id', String(projectId))
      let projectName = this.projectsCatalog
        .projects
        .find((proj) => proj.id == projectId)!
        .name
      this.cookieService.set('application-project-name', projectName)
    }
    this.router.navigateByUrl(url)
  }

  private refreshCatalog(): void {
    this.isCatalogLoading = true
    let query = new ProjectsQueryBindingModel()
    query.page = this.paginator.currentPage
    query.pageSize = this.pageSize
    if (this.searchName !== '') {
      query.searchName = this.searchName
    }
    query.channelId = this.selectedChannelId
    query.city = this.selectedCityName
    if (this.onlyAvailable) {
      query.onlyAvailable = true
    }
    if (this.onlyFee) {
      query.onlyFee = this.onlyFee
    }
    this.projectService.getProjectsCatalog(query).subscribe({
      next: (catalog) => {
        this.projectsCatalog = catalog
        this.paginator.totalPages = catalog.pagesCount
        this.isCatalogLoading = false
      }
    })
  }

  private synchronizeComponentWithUrl(): void {
    let queryParams = new URL(location.href).searchParams
    if (queryParams.has('page')) {
      this.paginator.currentPage = Number(queryParams.get('page'))
    }
    if (queryParams.has('searchName')) {
      this.searchName = queryParams.get('searchName')!
    }
    if (queryParams.has('channelId')) {
      this.selectedChannelId = Number(queryParams.get('channelId'))
    }
    if (queryParams.has('city')) {
      this.selectedCityName = queryParams.get('city')!
    }
    if (queryParams.has('onlyAvailable')) {
      this.onlyAvailable = Boolean(queryParams.get('onlyAvailable'))
    }
    if (queryParams.has('onlyFee')) {
      this.onlyFee = Boolean(queryParams.get('onlyFee'))
    }
  }

  private synchronizeUrlWithComponent(): void {
    let queryParams: { [key: string]: string } = {}
    queryParams['page'] = String(this.paginator.currentPage)
    if (this.searchName !== '') {
      queryParams['searchName'] = this.searchName
    }
    if (this.selectedChannelId !== null) {
      queryParams['channelId'] = String(this.selectedChannelId)
    }
    if (this.selectedCityName !== null) {
      queryParams['city'] = String(this.selectedCityName)
    }
    if (this.onlyAvailable) {
      queryParams['onlyAvailable'] = String(this.onlyAvailable)
    }
    if (this.onlyFee) {
      queryParams['onlyFee'] = String(this.onlyFee)
    }
    this.router.navigate(['/projects'], {queryParams})
  }

  private loadChannelsList(): void {
    this.isChannelsSelectLoading = true
    this.channelService.getChannelsListForSelect().subscribe({
      next: (channels) => {
        this.channels = channels
        this.isChannelsSelectLoading = false
      }
    })
  }

  private loadCitiesList(): void {
    this.isCitiesSelectLoading = true
    this.projectService.getCitiesListForSelect().subscribe({
      next: (cities) => {
        for (let city of cities) {
          this.cities.push({name: city})
        }
        this.isCitiesSelectLoading = false
      }
    })
  }

}
