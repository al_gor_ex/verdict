import {Component, OnInit} from '@angular/core';
import { EChartsOption } from 'echarts';
import {StatisticsService} from "../services/statistics.service";
import {StatisticsViewModel} from "../services/dto/view/statistics-view-model";

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  lineChartOptions: EChartsOption = {
    tooltip: {},
    xAxis: {
      data: [],
      silent: true,
      splitLine: {
        show: false
      },
      axisLabel: {
        fontSize: 16,
        color: '#000000'
      }
    },
    yAxis: {
      axisLabel: {
        fontSize: 16,
        color: '#000000'
      }
    },
    series: [
      {
        type: 'line',
        data: []
      },
    ]
  };

  pieChartOptions: EChartsOption = {
    tooltip: {},
    series: [
      {
        type: "pie",
        data: [],
        label: {
          fontSize: 16
        }
      }
    ]
  };

  pieChartColorTheme: object = {
    color: [
      '#b21ab4',
      '#6f0099',
      '#2a2073',
      '#0b5ea8',
      '#17aecc',
      '#b3b3ff',
      '#eb99ff'
    ]
  }

  constructor(
    private statisticsService: StatisticsService
  ) {
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.refreshChartsData()
  }

  private refreshChartsData(): void {
    this.statisticsService.getStatsForChats().subscribe({
      next: (stats) => {
        this.refreshLineChartData(stats)
        this.refreshPieChartData(stats)
      }
    })
  }

  private refreshLineChartData(stats: StatisticsViewModel): void {
    // Make a copy of chart options
    let newLineChartOptions = Object.assign({}, this.lineChartOptions)
    // @ts-ignore
    newLineChartOptions.xAxis.data = []
    // @ts-ignore
    newLineChartOptions.series[0].data = []
    stats.newApplicationsPerDay.map((dayData) => {
      // @ts-ignore
      newLineChartOptions.xAxis.data.push(dayData.date)
      // @ts-ignore
      newLineChartOptions.series[0].data.push(dayData.count)
    })
    this.lineChartOptions = newLineChartOptions
  }

  private refreshPieChartData(stats: StatisticsViewModel): void {
    // Make a copy of chart options
    let newPieChartOptions = Object.assign({}, this.pieChartOptions)
    // @ts-ignore
    newPieChartOptions.series[0].data = []
    stats.mostPopularChannels.map((piece) => {
      // @ts-ignore
      newPieChartOptions.series[0].data.push({
        name: piece.name,
        value: piece.percent
      })
    })
    this.pieChartOptions = newPieChartOptions
  }
}
