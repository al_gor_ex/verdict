import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainComponent } from './main/main.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ChannelsComponent } from './channels/channels.component';
import { PaginationComponent } from './pagination/pagination.component';
import { ProjectsComponent } from './projects/projects.component';
import { ApplicationsComponent } from './applications/applications.component';
import { ApplicationVideoComponent } from './application-video/application-video.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import { NewApplicationComponent } from './new-application/new-application.component';
import { NewEditProjectComponent } from './new-edit-project/new-edit-project.component';
import {FormsModule} from "@angular/forms";
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import {NgxEchartsModule} from "ngx-echarts";
import {NgSelectModule} from "@ng-select/ng-select";
import {HttpClientModule} from "@angular/common/http";
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";
import {CookieService} from "ngx-cookie-service";
import {SpinnersAngularModule} from "spinners-angular";
import {IsUnauthorizedGuard} from "./guards/is-unauthorized.guard";
import {IsAuthorizedGuard} from "./guards/is-authorized.guard";
import {IsParticipantGuard} from "./guards/is-participant.guard";
import {IsManagerGuard} from "./guards/is-manager.guard";
import {IsAdminGuard} from "./guards/is-admin.guard";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    NotFoundComponent,
    ChannelsComponent,
    PaginationComponent,
    ProjectsComponent,
    ApplicationsComponent,
    ApplicationVideoComponent,
    LoginComponent,
    RegistrationComponent,
    NewApplicationComponent,
    NewEditProjectComponent,
    AdminPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CKEditorModule,
    FormsModule,
    NgxEchartsModule.forRoot({
      echarts: import('echarts')
    }),
    NgSelectModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    SpinnersAngularModule
  ],
  providers: [
    CookieService,
    IsUnauthorizedGuard,
    IsAuthorizedGuard,
    IsParticipantGuard,
    IsManagerGuard,
    IsAdminGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
