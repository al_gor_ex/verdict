import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Role} from "../services/enums/role";
import {UserService} from "../services/user.service";
import {UserViewModel} from "../services/dto/view/user-view-model";
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  role = Role
  currentRole: Role | null = null
  userName: string = ''
  userPhoto: string = ''

  constructor(
    public router: Router,
    private userService: UserService,
    private cookieService: CookieService
  ) {
    userService.currentUserUpdateEvent.subscribe({
      next: (user: UserViewModel) => this.updateCurrentUser(user)
    })
  }

  ngOnInit(): void {
    this.restoreCurrentUser()
  }

  logout(): void {
    this.userService.logout().subscribe({
      next: () => {
        this.currentRole = null
        this.router.navigateByUrl('/main')
      }
    })
  }

  private restoreCurrentUser(): void {
    let role = this.userService.getCurrentUserRole()
    if (role !== null) {
      this.currentRole = role
      this.userName = this.cookieService.get('user-name')
      if (role !== Role.Admin) {
        this.userPhoto = window.localStorage.getItem('user-photo')!
      }
    }
  }

  private updateCurrentUser(user: UserViewModel): void {
    this.currentRole = user.role
    if (user.role !== Role.Admin) {
      this.userName = user.name
      this.userPhoto = user.photo
    }
  }

}
