import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {StatisticsViewModel} from "./dto/view/statistics-view-model";
import {Observable} from "rxjs";
import {apiUrl} from "../../environments/environment";

@Injectable({providedIn: 'root'})
export class StatisticsService {

  private httpClientOptions = {
    headers: {
      Accept: 'application/json'
    },
    withCredentials: true
  };

  constructor(private httpClient: HttpClient) {  }

  getStatsForChats(): Observable<StatisticsViewModel> {
    return this.httpClient.get<StatisticsViewModel>(`${apiUrl}/stats`, this.httpClientOptions)
  }
}
