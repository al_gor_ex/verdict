export class ProjectsQueryBindingModel {
  page: number
  pageSize: number
  searchName: string | null = null
  channelId: number | null = null
  city: string | null = null
  onlyAvailable: boolean | null = null
  onlyFee: boolean | null = null
}
