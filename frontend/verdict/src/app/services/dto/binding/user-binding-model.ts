import {Role} from "../../enums/role"
import {Gender} from "../../enums/gender"

export class UserBindingModel {
    role: Role
    name: string
    email: string
    password: string
    password_confirmation: string
    photo: string
    city: string | null = null
    gender: Gender | null = null
    birthDate: string | null = null
    channelName: string | null = null
    channelDescription: string | null = null
    channelPhotos: string[] | null = null
}
