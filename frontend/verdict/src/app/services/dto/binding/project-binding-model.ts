export class ProjectBindingModel {
  name: string
  photos: string[] = []
  description: string
  city: string
  participantsLimit: number
  fee: number | null
  startsAt: string
}
