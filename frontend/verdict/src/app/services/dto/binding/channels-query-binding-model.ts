export class ChannelsQueryBindingModel {
  page: number
  pageSize: number
  searchName: string | null = null
}
