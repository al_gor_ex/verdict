export class CredentialsBindingModel {
  constructor(
    public email: string,
    public password: string
  ) {
  }
}
