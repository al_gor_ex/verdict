import {Role} from "../../enums/role";

export class UserViewModel {
  name: string
  role: Role
  photo: string
}
