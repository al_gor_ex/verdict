export class ProjectEditorViewModel {
  name: string
  description: string
  city: string
  approvedParticipantsCount: number
  participantsLimit: number
  fee: number | null
  startsAt: string
}
