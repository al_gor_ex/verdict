export class ProjectsListViewModel {
  pagesCount: number
  projects: {
    id: number
    name: string
    description: string
    photos: string[]
    channelName: string
    city: string
    startsAt: string
    participantsCount: {
      approved: number
      limit: number
    }
    fee: number | null
    isAvailable: boolean | null
    hasApplied: boolean | null
    canEdit: boolean | null
  }[]
}
