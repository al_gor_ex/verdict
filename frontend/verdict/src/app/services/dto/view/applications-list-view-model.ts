import {ApplicationStatus} from "../../enums/application-status";
import {Gender} from "../../enums/gender";

export class ApplicationsListViewModel {
  applications: {
    id: number
    createdAt: string
    status: ApplicationStatus
    projectName: string
    description: string
    videoId: string
    manager: {
      name: string
      channelName: string
      photo: string
    } | null
    participant: {
      name: string
      email: string
      gender: Gender
      age: number,
      city: string,
      photo: string
    } | null
  }[]
}
