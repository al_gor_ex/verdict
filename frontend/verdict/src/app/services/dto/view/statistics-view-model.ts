export class StatisticsViewModel {
  newApplicationsPerDay: {
    date: string
    count: number
  }[]
  mostPopularChannels: {
    name: string
    percent: number
  }[]
}
