export class ChannelsListViewModel {
  pagesCount: number
  channels: {
    id: number
    name: string
    description: string
    photos: string[]
  }[]
}
