import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ChannelsQueryBindingModel} from "./dto/binding/channels-query-binding-model";
import {Observable} from "rxjs";
import {ChannelsListViewModel} from "./dto/view/channels-list-view-model";
import {apiUrl} from "../../environments/environment";
import {ChannelSelectViewModel} from "./dto/view/channel-select-view-model";

@Injectable({providedIn: "root"})
export class ChannelService {

  private httpClientOptions = {
    headers: {
      Accept: 'application/json'
    },
    withCredentials: true
  };

  constructor(private httpClient: HttpClient) {  }

  getChannelsCatalog(query: ChannelsQueryBindingModel): Observable<ChannelsListViewModel> {
    let url = new URL(`${apiUrl}/channels`)
    url.searchParams.append('page', String(query.page))
    url.searchParams.append('pageSize', String(query.pageSize))
    if (query.searchName !== null) {
      url.searchParams.append('searchName', query.searchName)
    }
    return this.httpClient.get<ChannelsListViewModel>(url.href, this.httpClientOptions)
  }

  getChannelsListForSelect(): Observable<ChannelSelectViewModel[]> {
    return this.httpClient.get<ChannelSelectViewModel[]>(`${apiUrl}/select/channels`, this.httpClientOptions)
  }
}
