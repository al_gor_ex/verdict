import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ApplicationBindingModel} from "./dto/binding/application-binding-model";
import {Observable} from "rxjs";
import {apiUrl} from "../../environments/environment";
import {NewApplicationViewModel} from "./dto/view/new-application-view-model";
import {ApplicationsQueryBindingModel} from "./dto/binding/applications-query-binding-model";
import {ApplicationsListViewModel} from "./dto/view/applications-list-view-model";
import {VideoViewModel} from "./dto/view/video-view-model";

@Injectable({providedIn: 'root'})
export class ApplicationService {

  private httpClientOptions = {
    headers: {
      Accept: 'application/json'
    },
    withCredentials: true
  };

  constructor(private httpClient: HttpClient) {
  }

  createApplication(application: ApplicationBindingModel): Observable<NewApplicationViewModel> {
    return this.httpClient.post<NewApplicationViewModel>(
      `${apiUrl}/applications`,
      application,
      this.httpClientOptions
    )
  }

  getApplicationsCatalog(query: ApplicationsQueryBindingModel): Observable<ApplicationsListViewModel> {
    let url = new URL(`${apiUrl}/applications`)
    url.searchParams.append('sort', query.sort)
    if (query.onlyPending !== null) {
      url.searchParams.append('onlyPending', String(query.onlyPending))
    }
    return this.httpClient.get<ApplicationsListViewModel>(
      url.href,
      this.httpClientOptions
    )
  }

  getApplicationVideo(videoId: string): Observable<VideoViewModel> {
    return this.httpClient.get<VideoViewModel>(`${apiUrl}/videos/${videoId}`, this.httpClientOptions)
  }

  cancelApplication(id: number): Observable<any> {
    return this.httpClient.post(`${apiUrl}/applications/${id}/cancel`, null, this.httpClientOptions)
  }

  approveApplication(id: number): Observable<any> {
    return this.httpClient.post(`${apiUrl}/applications/${id}/approve`, null, this.httpClientOptions)
  }

  declineApplication(id: number): Observable<any> {
    return this.httpClient.post(`${apiUrl}/applications/${id}/decline`, null, this.httpClientOptions)
  }
}
