export enum Role {
  Participant = 'participant',
  Manager = 'manager',
  Admin = 'admin'
}
