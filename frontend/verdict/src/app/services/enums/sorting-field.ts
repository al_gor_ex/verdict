export enum SortingField {
  CreatedAt = 'created_at',
  ParticipantName = 'participant_name',
  ProjectName = 'project_name',
  Status = 'status'
}
