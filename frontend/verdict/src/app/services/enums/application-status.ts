export enum ApplicationStatus {
  Approved = 'approved',
  Canceled = 'canceled',
  Declined = 'declined',
  Pending = 'pending'
}
