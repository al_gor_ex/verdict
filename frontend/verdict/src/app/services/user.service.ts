import {EventEmitter, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {UserBindingModel} from "./dto/binding/user-binding-model";
import {Observable} from "rxjs";
import {apiUrl, authUrl} from "../../environments/environment";
import {UserViewModel} from "./dto/view/user-view-model";
import {CookieService} from "ngx-cookie-service";
import {CredentialsBindingModel} from "./dto/binding/credentials-binding-model";
import {Role} from "./enums/role";

@Injectable({providedIn: 'root'})
export class UserService {

  currentUserUpdateEvent: EventEmitter<UserViewModel> = new EventEmitter<UserViewModel>()
  private httpClientOptions = {
    headers: {
      Accept: 'application/json'
    },
    withCredentials: true
  };

  constructor(private httpClient: HttpClient, private cookieService: CookieService) {  }

  registerNewUser(user: UserBindingModel): Observable<any> {
    return this.httpClient.post(`${authUrl}/register`, user, this.httpClientOptions)
  }

  updateCurrentUser(): void {
    this.httpClient.get<UserViewModel>(`${apiUrl}/user`, this.httpClientOptions).subscribe({
      next: (user) => {
        this.cookieService.set('user-role', user.role)
        this.cookieService.set('user-name', user.name)
        if (user.role !== Role.Admin) {
          window.localStorage.setItem('user-photo', user.photo)
        }
        this.currentUserUpdateEvent.emit(user)
      }
    })
  }

  getCurrentUserRole(): Role | null {
    if (!this.cookieService.check('user-role')) {
      return null
    }
    return this.cookieService.get('user-role') as Role
  }

  login(credentials: CredentialsBindingModel): Observable<any> {
    return this.httpClient.post(`${authUrl}/login`, credentials, this.httpClientOptions)
  }

  logout(): Observable<any> {
    this.cookieService.delete('user-role', '/')
    this.cookieService.delete('user-name', '/')
    window.localStorage.removeItem('user-photo')
    return this.httpClient.post(`${authUrl}/logout`, null, this.httpClientOptions)
  }
}
