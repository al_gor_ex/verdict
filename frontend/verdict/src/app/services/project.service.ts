import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ProjectEditorViewModel} from "./dto/view/project-editor-view-model";
import {apiUrl} from "../../environments/environment";
import {ProjectBindingModel} from "./dto/binding/project-binding-model";
import {ProjectsListViewModel} from "./dto/view/projects-list-view-model";
import {ProjectsQueryBindingModel} from "./dto/binding/projects-query-binding-model";

@Injectable({providedIn: "root"})
export class ProjectService {

  private httpClientOptions = {
    headers: {
      Accept: 'application/json'
    },
    withCredentials: true
  };

  constructor(private httpClient: HttpClient) {  }

  getProjectForEditing(id: number): Observable<ProjectEditorViewModel> {
    return this.httpClient.get<ProjectEditorViewModel>(`${apiUrl}/projects/${id}/edit`, this.httpClientOptions)
  }

  getCitiesListForSelect(): Observable<string[]> {
    return this.httpClient.get<string[]>(`${apiUrl}/select/cities`, this.httpClientOptions)
  }

  getProjectsCatalog(query: ProjectsQueryBindingModel): Observable<ProjectsListViewModel> {
    let url = new URL(`${apiUrl}/projects`)
    url.searchParams.append('page', String(query.page))
    url.searchParams.append('pageSize', String(query.pageSize))
    if (query.searchName !== null) {
      url.searchParams.append('searchName', query.searchName)
    }
    if (query.channelId !== null) {
      url.searchParams.append('channelId', String(query.channelId))
    }
    if (query.city !== null) {
      url.searchParams.append('city', query.city)
    }
    if (query.onlyAvailable !== null) {
      url.searchParams.append('onlyAvailable', String(query.onlyAvailable))
    }
    if (query.onlyFee !== null) {
      url.searchParams.append('onlyFee', String(query.onlyFee))
    }
    return this.httpClient.get<ProjectsListViewModel>(url.href, this.httpClientOptions)
  }

  createProject(project: ProjectBindingModel): Observable<any> {
    return this.httpClient.post(`${apiUrl}/projects`, project, this.httpClientOptions)
  }

  updateProject(id: number, newData: ProjectBindingModel): Observable<any> {
    return this.httpClient.put(`${apiUrl}/projects/${id}`, newData, this.httpClientOptions)
  }
}
