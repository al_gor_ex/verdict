import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(public router: Router, private userService: UserService) { }

  ngOnInit(): void {
    window.scrollTo({top: 0})
  }

  redirectManagerToProjects(): void {
    let route = (this.userService.getCurrentUserRole() === null) ? '/registration' : '/projects'
    this.router.navigateByUrl(route)
  }

}
