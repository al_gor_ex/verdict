import {Component, OnInit, ViewChild} from '@angular/core';
import {Role} from "../services/enums/role";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {CKEditorComponent} from "@ckeditor/ckeditor5-angular";
import * as EmailValidator from 'email-validator';
import {Gender} from "../services/enums/gender";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";
import {UserService} from "../services/user.service";
import {UserBindingModel} from "../services/dto/binding/user-binding-model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  email: string = ''
  name: string = ''
  password: string = ''
  passwordConfirmation: string = ''
  photo: string = ''
  city: string = ''
  gender = Gender
  selectedGender: Gender
  birthDate: string = ''
  channelName: string = ''
  channelDescription: string = ''
  channelPhotos: string[] = []
  role = Role
  selectedRole: Role
  showPasswords = false
  editor = ClassicEditor
  @ViewChild('channelDescriptionEditor') channelDescriptionEditor: CKEditorComponent | undefined;
  channelDescriptionEditorConfig: object = {
    toolbar: {
      items: [
        'undo',
        'redo',
        '|',
        'bold',
        'italic',
        '|',
        'numberedList',
        'bulletedList',
        '|',
        'link'
      ]
    },
    placeholder: 'Слоган, сферы деятельности, вклад в общественную культуру, награды, местоположение'
  }
  formErrors = {
    email: false,
    name: false,
    password: false,
    passwordConfirmation: false,
    photo: false,
    city: false,
    birthDate: false,
    channelName: false,
    channelDescription: false,
    channelPhotos: false
  }
  isBtnSubmitVisible = true
  @ViewChild('validationErrorSwal')
  validationErrorSwal: SwalComponent
  @ViewChild('registrationSuccessSwal')
  registrationSuccessSwal: SwalComponent
  @ViewChild('emailAlreadyTakenSwal')
  emailAlreadyTakenSwal: SwalComponent

  constructor(private userService: UserService, private router: Router) {
    this.selectedRole = Role.Participant
    this.selectedGender = Gender.Male
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.selectedRole = Role.Participant
    this.selectedGender = Gender.Male
  }

  onSumbit(): void {
    this.isBtnSubmitVisible = false
    this.validateRegistrationForm()
    if (this.anyFormErrors()) {
      this.validationErrorSwal.fire()
      this.isBtnSubmitVisible = true
      return
    }
    let user = new UserBindingModel()
    user.role = this.selectedRole
    user.name = this.name
    user.email = this.email
    user.password = this.password
    user.password_confirmation = this.passwordConfirmation
    user.photo = this.photo
    if (this.selectedRole == Role.Participant) {
      user.city = this.city
      user.gender = this.selectedGender
      user.birthDate = this.birthDate
    }
    if (this.selectedRole == Role.Manager) {
      user.channelName = this.channelName
      user.channelDescription = this.channelDescription
      user.channelPhotos = this.channelPhotos
    }
    this.userService.registerNewUser(user).subscribe({
        next: () => {
          this.registrationSuccessSwal.fire()
          this.userService.updateCurrentUser()
          this.isBtnSubmitVisible = true
          this.router.navigateByUrl('/applications')
        },
        error: (error) => {
          if (error.status == 422) {
            this.emailAlreadyTakenSwal.fire()
            this.isBtnSubmitVisible = true
          }
        }
      }
    )
  }

  redirectToLogin(): void {
    this.router.navigateByUrl('/login')
  }

  validateEmail(): void {
    this.formErrors.email = this.email == '' || !EmailValidator.validate(this.email)
  }

  validateName(): void {
    let isValid = true
    if (this.name == '' || this.name.length > 50) {
      isValid = false
    }
    let nameWords = this.name.split(' ')
    if (nameWords.length !== 2) {
      isValid = false
    } else {
      for (let word of nameWords) {
        if (!this.doesBeginWithCapitalLetter(word)) {
          isValid = false
          break
        }
      }
    }
    this.formErrors.name = !isValid
  }

  validatePassword(): void {
    this.formErrors.password = (this.password == '' || this.password.length < 8)
  }

  validatePasswordConfirmation(): void {
    this.formErrors.passwordConfirmation = (this.password !== this.passwordConfirmation)
  }

  validatePhoto(): void {
    let isValid = true
    let photoInput = document.getElementById('photo') as HTMLInputElement
    if (photoInput.files!.length !== 1) {
      isValid = false
    } else {
      for (let i = 0; i < photoInput.files!.length; i++) {
        if (!this.isPhotoValid(
          photoInput.files!.item(i)!,
          ['image/jpeg', 'image/png'],
          5
        )) {
          isValid = false
          break
        }
      }
    }
    this.formErrors.photo = !isValid
  }

  validateCity(): void {
    this.formErrors.city = (this.city == '' || this.city.length > 50)
  }

  validateBirthDate(): void {
    let today = new Date()
    today.setHours(0, 0, 0, 0)
    let pickedDate = new Date(this.birthDate)
    this.formErrors.birthDate = (this.birthDate == '' || pickedDate >= today)
  }

  validateChannelName(): void {
    this.formErrors.channelName = (this.channelName == '' || this.channelName.length > 50)
  }

  validateChannelDescription(): void {
    this.formErrors.channelDescription = (this.channelDescription.length > 750 || this.channelDescription == '')
  }

  validateChannelPhotos(): void {
    let isValid = true
    let photoInput = document.getElementById('channel-photos') as HTMLInputElement
    if (photoInput.files!.length > 3 || photoInput.files!.length == 0) {
      isValid = false
    } else {
      for (let i = 0; i < photoInput.files!.length; i++) {
        if (!this.isPhotoValid(
          photoInput.files!.item(i)!,
          ['image/jpeg', 'image/png'],
          5
        )) {
          isValid = false
          break
        }
      }
    }
    this.formErrors.channelPhotos = !isValid
  }

  extractInputFilesBase64(inputId: string): void {
    let input = document.getElementById(inputId) as HTMLInputElement
    // Clear old files
    if (inputId == 'channel-photos') {
      this.channelPhotos = []
    }
    for (let i = 0; i < input.files!.length; i++) {
      let file: File = input.files!.item(i)!
      let reader = new FileReader()
      reader.onloadend = () => {
        let fileContent = reader.result as string
        switch (inputId) {
          case 'photo':
            this.photo = fileContent
            break
          case 'channel-photos':
            this.channelPhotos.push(fileContent)
            break
        }
      }
      reader.readAsDataURL(file)
    }
  }

  private validateRegistrationForm(): void {
    this.validateEmail()
    this.validateName()
    this.validatePassword()
    this.validatePasswordConfirmation()
    this.validatePhoto()
    if (this.selectedRole == Role.Manager) {
      this.formErrors.city = false
      this.formErrors.birthDate = false
      this.validateChannelName()
      this.validateChannelDescription()
      this.validateChannelPhotos()
    }
    if (this.selectedRole == Role.Participant) {
      this.validateCity()
      this.validateBirthDate()
      this.formErrors.channelName = false
      this.formErrors.channelDescription = false
      this.formErrors.channelPhotos = false
    }
  }

  private anyFormErrors(): boolean {
    return Object.values(this.formErrors).includes(true)
  }

  private doesBeginWithCapitalLetter(word: string): boolean {
    let firstLetterCode = word.charCodeAt(0)
    // буквы кириллицы А-Я
    return (firstLetterCode >= 1040 && firstLetterCode <= 1071) ||
      // буква кириллицы Ё
      (firstLetterCode == 1025) ||
      // буквы латиницы A-Z
      (firstLetterCode >= 65 && firstLetterCode <= 90)
  }

  private isPhotoValid(photo: File, mimeTypes: string[], maxSizeMb: number): boolean {
    return !(
      !mimeTypes.includes(photo.type) ||
      photo.size > maxSizeMb * 1_048_576
    )
  }
}
