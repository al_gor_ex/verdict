import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../services/user.service";
import {Role} from "../services/enums/role";
import {SortingField} from "../services/enums/sorting-field";
import {SortingDirection} from "../services/enums/sorting-direction";
import {Router} from "@angular/router";
import {ApplicationsQueryBindingModel} from "../services/dto/binding/applications-query-binding-model";
import {ApplicationsListViewModel} from "../services/dto/view/applications-list-view-model";
import {ApplicationService} from "../services/application.service";
import {ApplicationStatus} from "../services/enums/application-status";
import {Gender} from "../services/enums/gender";
import {UserViewModel} from "../services/dto/view/user-view-model";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

  role = Role
  currentRole: Role
  sortingField = SortingField
  selectedSortingField: SortingField = SortingField.CreatedAt
  sortingDirection = SortingDirection
  selectedSortingDirection: SortingDirection = SortingDirection.Descending
  onlyPending: boolean = false
  isCatalogLoading: boolean = false
  applicationsCatalog: ApplicationsListViewModel
  applicationStatus = ApplicationStatus
  gender = Gender
  swalApplicationId: number = 0
  confirmationSwalOptions = {
    confirmButtonText: 'Да',
    cancelButtonText: 'Нет'
  }

  @ViewChild('confirmCancellationSwal')
  confirmCancellationSwal: SwalComponent
  @ViewChild('cancellationSuccessSwal')
  cancellationSuccessSwal: SwalComponent

  @ViewChild('confirmApprovalSwal')
  confirmApprovalSwal: SwalComponent
  @ViewChild('approvalSuccessSwal')
  approvalSuccessSwal: SwalComponent

  @ViewChild('confirmDeclinationSwal')
  confirmDeclinationSwal: SwalComponent
  @ViewChild('declinationSuccessSwal')
  declinationSuccessSwal: SwalComponent

  constructor(
    public router: Router,
    private userService: UserService,
    private applicationService: ApplicationService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    window.scrollTo({top: 0})
    this.currentRole = this.userService.getCurrentUserRole()!
    // Because applications component inits right after login, when role in cookies can be null.
    this.userService.currentUserUpdateEvent.subscribe({
      next: (user: UserViewModel) => {
        this.currentRole = user.role!
      }
    })
    this.synchronizeComponentWithUrl()
    this.synchronizeUrlWithComponent()
    this.refreshCatalog()
  }

  onSortingFieldChange(event: Event): void {
    this.selectedSortingField = (event.target as HTMLSelectElement).value as SortingField
    this.onFiltersChange()
  }

  toggleSortingDirection(): void {
    this.selectedSortingDirection = (this.selectedSortingDirection == SortingDirection.Ascending) ?
      SortingDirection.Descending :
      SortingDirection.Ascending
    this.onFiltersChange()
  }

  onFiltersChange(): void {
    this.synchronizeUrlWithComponent()
    this.refreshCatalog()
  }

  redirectToProjects(): void {
    this.router.navigateByUrl('/projects')
  }

  redirectToVideoPlayer(videoId: string): void {
    this.router.navigateByUrl(`/application-video/${videoId}`)
  }

  cancelApplication(id: number): void {
    this.swalApplicationId = id
    this.changeDetectorRef.detectChanges()
    this.confirmCancellationSwal.fire()
  }

  onCancellationConfirmed(id: number) {
    this.applicationService.cancelApplication(id).subscribe({
      next: () => {
        this.setApplicatonStatus(id, ApplicationStatus.Canceled)
        this.cancellationSuccessSwal.fire()
      }
    })
  }

  approveApplication(id: number): void {
    this.swalApplicationId = id
    this.changeDetectorRef.detectChanges()
    this.confirmApprovalSwal.fire()
  }

  onApprovalConfirmed(id: number) {
    this.applicationService.approveApplication(id).subscribe({
      next: () => {
        this.refreshAllApplicationsStatus()
        this.approvalSuccessSwal.fire()
      }
    })
  }

  declineApplication(id: number): void {
    this.swalApplicationId = id
    this.changeDetectorRef.detectChanges()
    this.confirmDeclinationSwal.fire()
  }

  onDeclinationConfirmed(id: number) {
    this.applicationService.declineApplication(id).subscribe({
      next: () => {
        this.setApplicatonStatus(id, ApplicationStatus.Declined)
        this.declinationSuccessSwal.fire()
      }
    })
  }

  private refreshCatalog(): void {
    this.isCatalogLoading = true
    let query = new ApplicationsQueryBindingModel()
    query.sort = `${this.selectedSortingField}:${this.selectedSortingDirection}`
    if (this.onlyPending) {
      query.onlyPending = this.onlyPending
    }
    this.applicationService.getApplicationsCatalog(query).subscribe({
      next: (applications) => {
        this.applicationsCatalog = applications
        this.isCatalogLoading = false
      },
      error: () => {
        this.isCatalogLoading = false
      }
    })
  }

  private refreshAllApplicationsStatus(): void {
    let query = new ApplicationsQueryBindingModel()
    query.sort = `${this.selectedSortingField}:${this.selectedSortingDirection}`
    if (this.onlyPending) {
      query.onlyPending = this.onlyPending
    }
    this.applicationService.getApplicationsCatalog(query).subscribe({
      next: (freshCatalog) => {
        for (let freshApplication of freshCatalog.applications) {
          this.setApplicatonStatus(freshApplication.id, freshApplication.status)
        }
      }
    })
  }

  private setApplicatonStatus(applicationId: number, newStatus: ApplicationStatus): void {
    let appIndex = this.applicationsCatalog
      .applications
      .findIndex(app => app.id == applicationId)
    if (appIndex !== -1) {
      this.applicationsCatalog.applications[appIndex].status = newStatus
    }
  }

  private synchronizeComponentWithUrl(): void {
    let queryParams = new URL(location.href).searchParams
    if (queryParams.has('sort')) {
      let sortParamParts: string[] = queryParams.get('sort')!.split(':')
      this.selectedSortingField = sortParamParts[0] as SortingField
      this.selectedSortingDirection = sortParamParts[1] as SortingDirection
    }
    if (queryParams.has('onlyPending')) {
      this.onlyPending = Boolean(queryParams.get('onlyPending'))
    }
  }

  private synchronizeUrlWithComponent(): void {
    let queryParams: { [key: string]: string } = {}
    queryParams['sort'] = `${this.selectedSortingField}:${this.selectedSortingDirection}`
    if (this.onlyPending) {
      queryParams['onlyPending'] = String(this.onlyPending)
    }
    this.router.navigate(['/applications'], {queryParams})
  }

}
