import {Component, OnInit, ViewChild} from '@angular/core';
import {ChannelsListViewModel} from "../services/dto/view/channels-list-view-model";
import {PaginationComponent} from "../pagination/pagination.component";
import {Router} from "@angular/router";
import {ChannelService} from "../services/channel.service";
import {ChannelsQueryBindingModel} from "../services/dto/binding/channels-query-binding-model";
import {debounceTime, Subject} from "rxjs";

@Component({
  selector: 'app-channels',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.css']
})
export class ChannelsComponent implements OnInit {

  pageSize: number = 2
  searchName: string = ''
  searchNameInputText: string = ''
  searchNameInputEvent = new Subject<void>()
  isLoading: boolean = false
  channelsCatalog: ChannelsListViewModel
  @ViewChild('paginator')
  paginator: PaginationComponent


  constructor(private router: Router, private channelService: ChannelService) {
  }

  ngOnInit(): void {
    window.scrollTo({top: 0})
  }

  ngAfterViewInit(): void {
    this.synchronizeComponentWithUrl()
    this.synchronizeUrlWithComponent()
    this.refreshCatalog()
    this.paginator.pageChangedEvent.subscribe(() => {
      this.synchronizeUrlWithComponent()
      this.refreshCatalog()
    })
    this.searchNameInputEvent
      .pipe(debounceTime(500))
      .subscribe({
        next: () => {
          this.searchName = this.searchNameInputText
          this.synchronizeUrlWithComponent()
          this.refreshCatalog()
        }
      })
  }

  redirectToProjects(channelId: number): void {
    this.router.navigate(['/projects'], {queryParams: {channelId}})
  }

  private refreshCatalog(): void {
    this.isLoading = true
    let query = new ChannelsQueryBindingModel()
    query.page = this.paginator.currentPage
    query.pageSize = this.pageSize
    if (this.searchName !== '') {
      query.searchName = this.searchName
    }
    this.channelService.getChannelsCatalog(query).subscribe({
      next: (catalog) => {
        this.channelsCatalog = catalog
        this.paginator.totalPages = catalog.pagesCount
        this.isLoading = false
      }
    })
  }

  private synchronizeComponentWithUrl(): void {
    let queryParams = new URL(location.href).searchParams
    if (queryParams.has('page')) {
      this.paginator.currentPage = Number(queryParams.get('page'))
    }
    if (queryParams.has('searchName')) {
      this.searchName = queryParams.get('searchName')!
    }
  }

  private synchronizeUrlWithComponent(): void {
    let queryParams: { [key: string]: string } = {}
    queryParams['page'] = String(this.paginator.currentPage)
    if (this.searchName !== '') {
      queryParams['searchName'] = this.searchName
    }
    this.router.navigate(['/channels'], {queryParams})
  }

}
