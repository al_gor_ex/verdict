import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {SwalComponent} from "@sweetalert2/ngx-sweetalert2";
import {UserService} from "../services/user.service";
import {CredentialsBindingModel} from "../services/dto/binding/credentials-binding-model";
import {Role} from "../services/enums/role";
import {UserViewModel} from "../services/dto/view/user-view-model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string = ''
  showPassword: boolean = false
  password: string = ''
  isBtnSubmitVisible: boolean = true
  @ViewChild('loginErrorSwal')
  loginErrorSwal: SwalComponent
  @ViewChild('validationErrorSwal')
  validationErrorSwal: SwalComponent

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit(): void {
    window.scrollTo({top: 0})
  }

  redirectToRegistration(): void {
    this.router.navigateByUrl('/registration')
  }

  handleEnterPress(ev: KeyboardEvent): void {
    if (ev.key == "Enter") {
      this.onSubmit()
    }
  }

  onSubmit(): void {
    this.isBtnSubmitVisible = false
    if (!this.isLoginFormValid()) {
      this.validationErrorSwal.fire()
      this.isBtnSubmitVisible = true
      return
    }
    this.userService.login(new CredentialsBindingModel(this.email, this.password)).subscribe({
      next: () => {
        this.userService.updateCurrentUser()
        this.userService.currentUserUpdateEvent.subscribe({
          next: (user: UserViewModel) => this.redirectByUserRole(user.role)
        })
      },
      error: (error) => {
        if (error.status == 422) {
          this.loginErrorSwal.fire()
        }
        this.isBtnSubmitVisible = true
      }
    })
  }

  private redirectByUserRole(role: Role): void {
    switch (role) {
      case Role.Manager:
      case Role.Participant:
        this.router.navigateByUrl('/applications')
        break;
      case Role.Admin:
        this.router.navigateByUrl('/admin-panel')
        break;
    }
  }

  private isLoginFormValid(): boolean {
    return this.email !== '' && this.password !== ''
  }
}
