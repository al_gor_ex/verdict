# Verdict - платформа подбора участников телевизионных проектов

Приложение предназначено для управления данными о:
- телеканалах
- проектах, проводимых телеканалами
- участниках проектов и представителях телеканалов
- заявках, подаваемых участниками на проекты, и решениях, вынесенных по заявкам представителями телеканалов

## Стек технологий

Frontend:

- HTML
- CSS
- Bootstrap
- TypeScript
- Angular

Backend:

- PHP
- Laravel
- MySQL
- Redis
- Sanctum + Fortify

## Перечень страниц фронтенд-приложения

![](https://sun9-46.userapi.com/impf/TexLqMEgcfaKh06KeB7MSKKagDJbB92FKStFgA/3DuwBHjs9sI.jpg?size=768x258&quality=95&sign=5b265e276d2221fcefe03ddd67bb7500&type=album)

## Описание API

Методы API документированы в формате Swagger (см. файл `/backend/verdict/routes/openapi.yaml`)

## ER-диаграмма

![](https://sun9-19.userapi.com/impf/R34QR2oKzyCNO6RMbMOiA8dJ7HjEZxBP16Khlw/S7eEpDj5hvI.jpg?size=1142x448&quality=95&sign=9347db5f8ee263853474f99ae56541ad&type=album)

## Скриншоты

Главная страница

![](https://sun9-39.userapi.com/impf/xLwFM8VFzOLxqP15f6qWAHDcIQ-YlmF2SKub0g/n9pr0s-AdyI.jpg?size=1349x768&quality=95&sign=875a474eb499d9d0f35e6f0925f344c1&type=album)
![](https://sun9-57.userapi.com/impf/yJH1HGUWvkWvLt2ibAs6nlHFLJu6ennC0vMjSA/DhUMiTa_EXE.jpg?size=1346x767&quality=95&sign=095929d9bf8d26410dcd95df415ec6a3&type=album)

Каталог телеканалов

![](https://sun9-77.userapi.com/impf/qH-UUu1kRJnhs_1BxSpOtN_XIsamraIrdFZ84w/QEFuk924AeM.jpg?size=1346x768&quality=95&sign=3d66fbe7e466d5b7b6bd6ed0c7af4f37&type=album)

Каталог проектов

![](https://sun9-9.userapi.com/impf/so1F8inZ3WT9C8E4JM-6OWdGdD9E4AEpMcnrYQ/CEf7HF3zqqU.jpg?size=1349x768&quality=95&sign=bf1d7724c5f6dbc9142035ee8e53810a&type=album)

Страница входа в аккаунт

![](https://sun9-7.userapi.com/impf/TZkOYlLubUFcDNjvZceW9WMT1VzPYIOsNp9AZw/JSvCZ-4yYbA.jpg?size=1344x768&quality=95&sign=5e35f2ee2ea04b31056130bbe935376e&type=album)

Страница регистрации

![](https://sun9-19.userapi.com/impf/qeJZKdoG6WD4JPMZExBwPF0l1ZH8yoaE5-8M7A/1jq9oV17njc.jpg?size=1346x768&quality=95&sign=7ff1351cc8f97a803ed76d435cb0df0b&type=album)

Каталог заявок

![](https://sun9-81.userapi.com/impf/9rawgzT7bL9xEhL6bEBQM-paUhX4KSLayTJPpQ/pxbIudM1xkg.jpg?size=1347x768&quality=95&sign=63b7e66d5052ce264016c3a6701350d1&type=album)

Просмотр видео заявки

![](https://sun9-76.userapi.com/impf/1CGx0ShRjcO-SH7Uu22YjI-3N8QjmcB6l3pXtg/OiwrLQGLorc.jpg?size=1346x620&quality=95&sign=5a323c6cb85e2d995826b1913bcfad20&type=album)

Создание заявки

![](https://sun9-24.userapi.com/impf/j1UXofsMIk_28GKaAxA6u-Im0BuZ7A0OI0ueog/N0r_HOSiFmo.jpg?size=1344x768&quality=95&sign=effe2721a3a47adcf7ac7d1173dc622f&type=album)

Редактирование проекта

![](https://sun9-9.userapi.com/impf/Y2vNSecwpvfSRH4ofxjumtLSqmxpCnCmU9vRmA/laEma2vrjfo.jpg?size=1345x768&quality=95&sign=e31a65530ba6d4c8ee071d4c1478a1f5&type=album)

Статистика в панели администратора

![](https://sun9-71.userapi.com/impf/7yJi62aCuck-NwboTJQ_tRVzn9s40swcEwvuqg/6_L_Fmv6CsA.jpg?size=1346x767&quality=95&sign=4e3a68b3a196e5ead667b37f6da456fe&type=album)

Страница 404

![](https://sun9-11.userapi.com/impf/dvSIYOEBRsk80thQMY3WewNng85IsC8kwCq7mQ/_lxW385HtQg.jpg?size=1366x623&quality=95&sign=f2ebeba4a6d3599c94f3adf4453c6519&type=album)